from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np

def basis_to_carthesian(basis, points):
    return np.tensordot(basis, points, [0,0])

def fractional_coords(n):
    i1 = np.linspace(0., 1., n[0], endpoint=False)
    i2 = np.linspace(0., 1., n[1], endpoint=False)
    i3 = np.linspace(0., 1., n[2], endpoint=False)
    return (i1, i2, i3)

def fractional_grid(n):
    i1, i2, i3 = fractional_coords(n)
    ii1, ii2, ii3 = np.meshgrid(i1, i2, i3, indexing='ij', copy=False)
    return np.array([ii1,ii2,ii3])

def basis_grid(basis, n):
    fgrid = fractional_grid(n)
    abs_basis = np.sqrt(np.sum(basis**2, 1))
    return fgrid * abs_basis[:,None,None,None]

def carthesian_grid(basis, n):
    fgrid = fractional_grid(n)
    return basis_to_carthesian(basis, fgrid)

def basis_coords(basis, n):
    i0, i1, i2 = fractional_coords(n)
    abs_basis = np.sqrt(np.sum(basis**2, 1))
    return (abs_basis[0] * i0, abs_basis[1] * i1, abs_basis[2] *i2)
