
def plt(bg=False):
    if bg:
        import matplotlib
        matplotlib.use('Agg')
    import matplotlib.pyplot as pyplot
    return pyplot

class PlotProperties(object):
    facecolor = 'white'
    size = None
    style = None
    context = None
    palette = None 
    dpi = 1000
    boundary_box = 'tight'

properties = PlotProperties()

def add_arguments_to(parser):
    group = parser.add_argument_group('plotting')
    group.add_argument('--plot-size', type=int, nargs=2, default=(8, 6), metavar=('WIDTH', 'HEIGHT'))
    group.add_argument('--plot-style', choices=['ticks', 'ticks-in', 'white', 'dark', 'whitegrid', 'darkgrid'])
    group.add_argument('--plot-context', choices=['paper', 'talk'])
    group.add_argument('--plot-palette', choices=['hls', 'husl', 'deep', 'muted', 'pastel', 
                                                  'bright', 'dark', 'colorblind'])

def figure(*args, **kwargs):
    import matplotlib.pyplot as plt
    kwargs['figsize'] = kwargs.get('figsize', properties.size)
    kwargs['facecolor'] = kwargs.get('facecolor', properties.facecolor)
    return plt.figure(*args, **kwargs)

def save(filename):
    import matplotlib.pyplot as plt
    plt.savefig(filename, dpi=properties.dpi, bbox_inches=properties.boundary_box)

def prettify():
    import matplotlib.pyplot as plt
    import seaborn as sns
    style = properties.style
    context = properties.context
    palette = properties.palette
    if style == 'ticks-in':
        sns.set_style('ticks', {'xtick.direction': 'in', 'ytick.direction': 'in'})
    elif style:
        sns.set_style(style)
    else:
        sns.set_style('ticks')
    if context:
        sns.set_context(context)
    if palette:
        sns.set_palette(palette)
    plt.rcParams['text.latex.preamble'] = [r"\usepackage{lmodern}"]
    params = {
            'text.usetex': True,
            'font.size': 11,
            'font.family': 'lmodern',
            'text.latex.unicode': True
            }
    plt.rcParams.update(params)

def parse_args(args):
    import argparse
    if isinstance(args, argparse.Namespace):
        properties.size = args.plot_size or properties.size
        properties.style = args.plot_style or properties.style
        properties.context = args.plot_context or properties.context
        properties.palette = args.plot_palette or properties.palette
    else:
        properties.size = args['--plot-size'] or properties.size
        properties.style = args['--plot-style'] or properties.style
        properties.context = args['--plot-context'] or properties.context
        properties.palette = args['--plot-palette'] or properties.palette

def tight_layout(fig=None):
    import warnings
    import matplotlib.pyplot as plt
    (fig or plt.gcf()).set_tight_layout(True)
    warnings.filterwarnings('ignore', 'This figure includes Axes that are not compatible with tight_layout, so its results might be incorrect.', UserWarning)
