from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import scipy.interpolate as interp
from . import constants as c
from . import wavecar
from .utility import fermi_dirac, delta_gauss, cdiff
from .caching import cached_property

class MatrixElement(object):
    '''Calculates matrix elements between ribbons in a structure.

    We optimized for iteration over the states of the second ribbon while the
    state of the first ribbon is kept constant. We then make proper use of a 
    precalculated grid and also keep the wavefunction of the first ribbon in memory.
    '''
    def __init__(self, structure, operator=None):
        self.structure = structure
        self.operator = operator
        self.from_ = 0
        self.to_ = 1

    @cached_property
    def relative_grid(self):
        return self.structure.map_grid(self.from_, self.to_)

    @cached_property
    def mask(self):
        return self.structure.overlap_mask(self.from_, self.to_)

    def bra(self, ik0, iband0):
        realwave = self.structure.ribbons[self.from_].realwave(ik0, iband0, 
                                                               operator=self.operator)
        self.wave0 = np.ravel(realwave).T[self.mask]
        return self

    def ket(self, ik1, iband1, plot=False):
        points = self.relative_grid[self.mask,:].T
        offset = wavecar.minimize_interpolation(self.structure.ribbons[self.to_].shape, points)
        points = points - offset[:,None]
        r_offset = offset * self.structure.ribbons[self.to_].big_absa
        realwave = self.structure.ribbons[self.to_].realwave(ik1, iband1, offset=r_offset)
        wave1 = wavecar.interpolate(realwave, points)

        if plot:
            import matplotlib.pyplot as plt
            overlap = np.abs(np.conjugate(self.wave0) * wave1)
            pgrid = self.structure.ribbons[self.from_].grid3
            poverlap = np.zeros(self.structure.grids[self.from_].shape[0])
            poverlap[self.mask] = overlap
            poverlap = np.reshape(poverlap, pgrid.shape[1:])
            pwave = np.zeros(self.structure.grids[self.from_].shape[0])
            pwave[self.mask] = np.abs(wave1)**2
            pwave = np.reshape(pwave, pgrid.shape[1:])
            plt.figure()
            plt.subplot(131)
            plt.title('Looking along X')
            plt.contourf(pgrid[1,0,:,:]/c.nm, pgrid[2,0,:,:]/c.nm, np.sum(poverlap, 0))
            plt.axis('equal')
            plt.subplot(132)
            plt.title('Looking along Y')
            plt.contourf(pgrid[0,:,0,:]/c.nm, pgrid[2,:,0,:]/c.nm, np.sum(poverlap, 1))
            plt.axis('equal')
            plt.subplot(133)
            plt.title('Looking along Z')
            plt.contourf(pgrid[0,:,:,0]/c.nm, pgrid[1,:,:,0]/c.nm, np.sum(poverlap, 2))
            plt.axis('equal')

        return np.vdot(self.wave0, wave1)

class BardeenTransfer(object):
    def __init__(self, structure):
        self.structure = structure
        self.overlap = MatrixElement(structure, operator='T')

    def is_relevant(self, E0, E1, dE_V, dE_kT):
        gaps = tuple(ribbon.gap for ribbon in self.structure.ribbons)
        midgaps = tuple(ribbon.midgap for ribbon in self.structure.ribbons)
        mE0 = E0 - midgaps[0]
        mE1 = E1 - midgaps[1]
        EG0, EG1 = (abs(gap) for gap in gaps)
        V_lim = (np.abs(mE0 - mE1) < np.max(EG0, EG1)/2 + dE_V)
        kT_lim = ((np.abs(mE0) < EG0/2 + dE_kT) + 
                  (np.abs(mE1) < EG1/2 + dE_kT) + 
                  (np.sign(mE0) != np.sign(mE1)))
        return V_lim * kT_lim

    def build_matrix(self, dE_V=1.*c.eV, dE_kT=0.5*c.eV, mpi_enabled=False, output=False):
        if mpi_enabled:
            from .mpitools import MPISession
            mpi = MPISession()
            
        if output:
            if mpi_enabled:
                if mpi.is_root: print(' ik0  band0  ik1  band1  |<0|1>|^2 ')
            else:
                print('  progress   ik0  band0  ik1  band1  |<0|1>|^2 ')

        E0, E1 = tuple(ribbon.E[ribbon.sort_bands] for ribbon in self.structure.ribbons)

        gaps = tuple(ribbon.gap for ribbon in self.structure.ribbons)
        midgaps = tuple(ribbon.midgap for ribbon in self.structure.ribbons)
        
        ii0 = np.array(np.nonzero(np.abs(E0 - midgaps[0]) < dE_V + gaps[0]/2)).T
        ii1 = np.array(np.nonzero(np.abs(E1 - midgaps[1]) < dE_V + gaps[1]/2)).T

        if mpi:
            n0_full = ii0.shape[0]
            def ii0_slice(rank):
                start = int((rank*n0_full)/mpi.size)
                end = int(((rank+1)*n0_full)/mpi.size)
                return slice(start, end)
            ii0_full = ii0
            ii0 = ii0[ii0_slice(mpi.rank),:]

        n0 = ii0.shape[0]
        n1 = ii1.shape[0]
        overlaps = np.empty((n0,n1), dtype=complex)
        E = np.empty((2,n0,n1), dtype=float)
        for i0, (ik0, iband0) in enumerate(ii0):
            uiband0 = self.structure.ribbons[0].sort_bands[1][ik0, iband0]
            self.overlap.bra(ik0, uiband0)
            for i1, (ik1, iband1) in enumerate(ii1):
                uiband1 = self.structure.ribbons[1].sort_bands[1][ik1, iband1]
                E[:,i0,i1] = (E0[ik0, iband0], E1[ik1, iband1])
                if self.is_relevant(E0[ik0, iband0], E1[ik1, iband1], dE_V, dE_kT):
                    overlaps[i0, i1] = self.overlap.ket(ik1, uiband1)
                else:
                    overlaps[i0, i1] = 0.0
                if output and mpi_enabled:
                    print('{:>5} {:>5} {:>5} {:>5} {:.3e}'
                            .format(ik0, iband0, ik1, iband1, 
                                    abs(overlaps[i0, i1])**2))
                if output and not mpi:
                    print('{:>5}/{:<5} {:>5} {:>5} {:>5} {:>5} {:.3e}'
                            .format(i0*n1+i1+1, n0*n1,
                                    ik0, iband0, ik1, iband1, 
                                    abs(overlaps[i0, i1])**2))
       
        if mpi:
            TAG_OVERLAPS = 13
            TAG_E = 14
            if not mpi.is_root:
                mpi.send(overlaps, dest=0, tag=TAG_OVERLAPS)
                mpi.send(E, dest=0, tag=TAG_E)
                return None

            else:
                tmp = overlaps
                overlaps = np.empty((n0_full,n1), dtype=overlaps.dtype)
                overlaps[ii0_slice(mpi.rank),:] = tmp
                tmp = E
                E = np.empty((2,n0_full,n1), dtype=E.dtype)
                E[:,ii0_slice(mpi.rank),:] = tmp
                for node in range(1, mpi.size):
                    tmp = np.empty_like(overlaps[ii0_slice(node),:])
                    mpi.recv(tmp, source=node, tag=TAG_OVERLAPS)
                    overlaps[ii0_slice(node),:] = tmp
                    tmp = np.empty_like(E[:,ii0_slice(node),:])
                    mpi.recv(tmp, source=node, tag=TAG_E)
                    E[:,ii0_slice(node),:] = tmp
                    self.ii0 = ii0_full

        self.ii1 = ii1
        self.E = E
        self.overlaps = overlaps

        if not mpi or mpi.is_root:
            print('done')
        
        return self

    def save_matrix(self, filename, mpi=False):
        rank = 0
        if mpi:
            from mpi4py import MPI
            rank = MPI.COMM_WORLD.Get_rank()
        if rank == 0:
            np.savez(filename, ii0=self.ii0, ii1=self.ii1, E=self.E, overlaps=self.overlaps)

    def load_matrix(self, filename): 
        store = np.load(filename if filename.endswith('.npz') else filename+'.npz')
        self.ii0 = store['ii0']
        self.ii1 = store['ii1']
        self.E = store['E']
        self.overlaps = store['overlaps']
        return self

    def plot_matrix(self, mu0, mu1):
        import warnings
        import matplotlib.pyplot as plt
        import matplotlib.gridspec as gridspec
        import matplotlib.widgets as widgets
        import seaborn as sns
        sns.set_style('white')
        sns.set_style('ticks', {'xtick.direction': 'in', 'ytick.direction': 'in'})
        s_cmap = plt.get_cmap('winter')
        f_cmap = plt.get_cmap('BrBG')

        fig = plt.figure(facecolor='#ffffff')
        gs = gridspec.GridSpec(2, 2, width_ratios=[1,4], height_ratios=[4,1],
                               wspace=0.0, hspace=0.0,
                               bottom=0.05, top=0.90,
                               left=0.05, right=0.85)
        gs_cb = gridspec.GridSpec(2, 1,
                                  wspace=0.0, hspace=0.05,
                                  bottom=0.05, top=0.90,
                                  left=0.88, right=0.92)
        bias_ax = fig.add_axes([0.10, 0.95, 0.75, 0.03])
        sbias = widgets.Slider(bias_ax, 'Bias', -2.0, 2.0, valinit=0.0)

        ax = fig.add_subplot(gs[1])
        M = np.abs(self.overlaps)**2
        M[M<1e-16] = 1e-16
        E0 = self.E[0,:,:] - mu0
        E1 = self.E[1,:,:] - mu1
        deltaE = np.array([max(E0.min(), E1.min()), min(E0.max(), E1.max())])
        E0_lin = np.linspace(E0.min(), E0.max(), 400)
        E1_lin = np.linspace(E1.min(), E1.max(), 400)
        EE0, EE1 = np.meshgrid(E0_lin, E1_lin, indexing='ij')
        filling = fermi_dirac(EE0, 300*c.K) - fermi_dirac(EE1, 300*c.K)
        levels = np.linspace(-1., 1., 100)
        k0 = self.structure.ribbons[0].k[self.ii0[:,0]]
        k1 = self.structure.ribbons[1].k[self.ii1[:,0]]
        dk = k0[:,None,:] - k1[None,:,:]
        absdk = np.sqrt(np.sum(dk**2, 2))
        f = ax.contourf(EE0/c.eV, EE1/c.eV, filling, cmap=f_cmap, levels=levels)
        dl, = ax.plot(deltaE/c.eV, deltaE/c.eV-0.0, color='#ee0000')
        def update(bias):
            dl.set_xdata(deltaE/c.eV + bias)
            fig.canvas.draw_idle()
        sbias.on_changed(update)
        s = ax.scatter(E0/c.eV, E1/c.eV,
                       s=100*M/M.max(),
                       c=M,
                       marker='o', cmap=s_cmap)
        ax.set_aspect('equal')
        plt.setp(ax.get_xticklabels(), visible=False)
        plt.setp(ax.get_yticklabels(), visible=False)

        ax1 = fig.add_subplot(gs[0], sharey=ax)
        ks, polylines, E_ranges = self.structure.ribbons[1].interpolated_bands
        kk = np.linspace(ks[0], ks[-1], 1000)
        for ib in range(len(polylines)):
            if self.E[1,:,:].max() < E_ranges[ib,0] or E_ranges[ib,1] < self.E[1,:,:].min():
                continue
            E = self.structure.ribbons[1].E[self.structure.ribbons[1].sort_bands]
            ax1.scatter(ks, (E[:, ib]-mu1)/c.eV)
            line, = ax1.plot(kk, (polylines[ib](kk)-mu1)/c.eV)
            ax1.axhspan((E_ranges[ib,0]-mu1)/c.eV, (E_ranges[ib,1]-mu1)/c.eV, 
                        alpha=0.2, color=line.get_color())
        ax1.set_xlim((kk.min(), kk.max()))
        ax1.set_ylabel(r'$E_1$')

        ax0 = fig.add_subplot(gs[3], sharex=ax)
        ks, polylines, E_ranges = self.structure.ribbons[0].interpolated_bands
        kk = np.linspace(ks[0], ks[-1], 1000)
        for ib in range(len(polylines)):
            if self.E[0,:,:].max() < E_ranges[ib,0] or E_ranges[ib,1] < self.E[0,:,:].min():
                continue
            E = self.structure.ribbons[0].E[self.structure.ribbons[0].sort_bands]
            ax0.scatter((E[:, ib]-mu0)/c.eV, ks)
            line = ax0.plot((polylines[ib](kk)-mu0)/c.eV, kk)[0]
            ax0.axvspan((E_ranges[ib,0]-mu0)/c.eV, (E_ranges[ib,1]-mu0)/c.eV,
                        alpha=0.2, color=line.get_color())
        ax0.set_ylim((kk.min(), kk.max()))
        ax0.set_xlabel(r'$E_0$')

        ax_cb0 = fig.add_subplot(gs_cb[0])
        plt.colorbar(s, cax=ax_cb0)
        ax_cb1 = fig.add_subplot(gs_cb[1])
        plt.colorbar(f, cax=ax_cb1)
        plt.show()
        warnings.filterwarnings("ignore")

    def current(self, mu0, mu1, Vds, dE=None, T=300*c.K, oversampling=5):
        Nk0, Nk1 = tuple(oversampling * ribbon.nk for ribbon in self.structure.ribbons)

        k0, Ek0, Erange0 = self.structure.ribbons[0].interpolated_bands
        k1, Ek1, Erange1 = self.structure.ribbons[1].interpolated_bands

        k_fine0, dk0 = np.linspace(k0.min(), k0.max(), Nk0, retstep=True)
        k_fine1, dk1 = np.linspace(k1.min(), k1.max(), Nk1, retstep=True)

        kk = np.prod([ribbon.b_lin for ribbon in self.structure.ribbons])
        prefactor = -2 * np.pi * dk0 * dk1 / kk

        Vds = np.asarray(Vds) 
        J = np.zeros(Vds.shape)
        for iband0 in set(self.ii0[:,1]):
            select0 = self.ii0[:,1] == iband0
            s_k0 = k0[self.ii0[select0, 0]]
            E0 = Ek0[iband0](k_fine0) - mu0
            f0 = fermi_dirac(E0, T)
            for iband1 in set(self.ii1[:,1]):
                select1 = self.ii1[:,1] == iband1
                M = np.abs(self.overlaps[np.ix_(select0, select1)])**2
                # ignore if no non-zero matrix elements are present
                # and also if M.shape <= 1 in one or two of the k-directions (only one point)
                if np.any(M != 0) and np.all(np.array(M.shape) > 1):
                    s_k1 = k1[self.ii1[select1, 0]]
                    E1 = Ek1[iband1](k_fine1) - mu1
                    f1 = fermi_dirac(E1, T)
                    f_overlap = interp.interp2d(s_k0, s_k1, M.T, # yes, .T due to scipy weirdness
                                                kind='linear', fill_value=0.0)
                    EE0, EE1 = np.meshgrid(E0, E1, indexing='ij', copy=False)
                    ff0, ff1 = np.meshgrid(f0, f1, indexing='ij', copy=False)
                    if dE is None:
                        dE0, dE1 = np.abs(cdiff(E0)), np.abs(cdiff(E1))
                        dEE0, dEE1 = np.meshgrid(dE0, dE1, indexing='ij')
                        dE = (dEE0 + dEE1) + 0.005*c.eV
                    partial = (ff0 - ff1) * f_overlap(k_fine0, k_fine1).T # .T due to scipy weirdness 
                    J += np.array([np.sum(partial * delta_gauss((EE0-Vi) - EE1, dE)) 
                                        for Vi in Vds])
        return prefactor * J
