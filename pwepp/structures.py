from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import numpy.linalg as la
from . import constants as c
from .ribbon import Ribbon
from .caching import cached_property
from .utility import hex2rgb

def sides(cell):
    origin = cell[0,:]
    cv = (cell[1:,:] - cell[0,:])
    sides = []
    for i in range(3):
        sides += [
            np.array([[0,0,0], cv[i,:]]),
            np.array([cv[i%3,:] + cv[(i+1)%3,:], cv[0,:] + cv[1,:] + cv[2,:]]),
            np.array([cv[i,:], cv[i,:] + cv[(i+1)%3,:]]),
            np.array([cv[(i+1)%3,:], cv[i,:] + cv[(i+1)%3,:]])
            ]
    return [origin + side for side in sides]

class PlacementError(Exception):
    pass

class LayeredRibbons(object):
    def __init__(self):
        self.ribbons = [None, None]
        self.positions = [None, None]

    def __str__(self):
        return '\n'.join(['{}:'.format(self.__class__.__name__)] +
                         ['  {}. {} @ {}'.format(i, str(ribbon), str(position)) 
                             for i, (ribbon, position) 
                             in enumerate(zip(self.ribbons, self.positions))])

    def place(self, ribbon, position, index=None):
        if not isinstance(ribbon, Ribbon):
            raise TypeError('ribbon should be of type Ribbon, got'.format(type(ribbon)))
        if index is None:
            try:
                index = [ii for ii, r in enumerate(self.ribbons) if r is None][0]
            except IndexError:
                raise PlacementError('all layers are already placed, specify index to override')
        if index not in (0,1):
            raise ValueError('index must be 0 or 1, got {}'.format(index))
        self.ribbons[index] = ribbon
        self.positions[index] = position
        del self.atoms
        del self.cells
        del self.grids
        return self

    @cached_property
    def atoms(self):
        all_atoms = []
        for ribbon, position in zip(self.ribbons, self.positions):
            atoms = ribbon.atoms.copy()
            pos = atoms.get_positions() * c.Ang
            pos = position(pos)
            atoms.set_positions(pos / c.Ang)
            all_atoms.append(atoms)
        return all_atoms

    @cached_property
    def cells(self):
        cells = []
        for ribbon, position in zip(self.ribbons, self.positions):
            cell = np.concatenate(([[0,0,0]], ribbon.atoms.get_cell() * c.Ang))
            cells.append(position(cell))
        return cells

    @cached_property
    def grids(self):
        grids = []
        for ribbon, position in zip(self.ribbons, self.positions):
            grids.append(position(ribbon.grid))
        return grids

    def map_grid(self, from_, to_):
        cell = self.cells[to_]
        grid = self.grids[from_]
        origin = cell[None,0,:]
        relative_b = la.inv(cell[1:,:] - origin)
        return np.dot(grid - origin, relative_b)

    def overlap_mask(self, from_, to_):
        mapped_grid = self.map_grid(from_, to_)
        return np.all(np.logical_and(0 <= mapped_grid, mapped_grid <= 1), 1)

    def show(self, backend='ase', **kwargs):
        if backend == 'ase':
            self.show_ase(**kwargs)
        elif backend == 'mayavi':
            self.show_mayavi(**kwargs)

    def show_ase(self):
        import ase.visualize
        os.environ["LC_ALL"] = "C"
        atoms = ase.atoms.Atoms()
        for atoms in self.atoms:
            atoms += ribbon.atoms
        ase.visualize.view(atoms)

    def show_mayavi(self, show_grid=False, show_cell=True, 
                          blocking=True, figure=None, alpha=1.0):
        import os
        os.environ["ETS_TOOLKIT"] = "qt4"
        import mayavi.mlab as mlab
        import numpy.random as random
        from . import cpk
        f = figure or mlab.figure(bgcolor=hex2rgb('#ffffff'), size=(800, 600))
        for atoms, cell in zip(self.atoms, self.cells):
            chems = np.array(atoms.get_chemical_symbols())
            for chem in set(chems):
                color = cpk.color(chem)
                size = cpk.size(chem)
                pos = atoms[chem == chems].get_positions()
                x, y, z = pos[:,0], pos[:,1], pos[:,2]
                mlab.points3d(x, y, z, color=hex2rgb(color),
                              resolution=30, opacity=alpha,
                              scale_factor=np.sqrt(size)/c.Ang)
            if show_cell:
                for side in sides(cell):
                    mlab.plot3d(side[:,0]/c.Ang, side[:,1]/c.Ang, side[:,2]/c.Ang,
                                opacity=alpha, color=hex2rgb('#000000'), tube_radius=0.1)
        mlab.points3d([0.], [0.], [0.], color=hex2rgb("#ff0000"),
                      resolution=30, opacity=1.0,
                      scale_factor=0.3)
        if show_grid:
            for igrid, grid in enumerate(self.grids):
                mask = self.overlap_mask(igrid, (igrid+1)%2)
                sample = random.randint(0, grid[mask,:].shape[0], (10000,))
                x, y, z = grid[mask,:][sample,0], grid[mask,:][sample,1], grid[mask,:][sample,2]
                pts = mlab.pipeline.scalar_scatter(x/c.Ang, y/c.Ang, z/c.Ang,
                                                   0.1*np.ones(x.shape))
                mlab.pipeline.volume(mlab.pipeline.gaussian_splatter(pts))
        f.scene.camera.parallel_projection = True

        if blocking:
            mlab.show()
        return f

    def show_matplotlib(layers, show_grid=None, show_cell=True):
        from mpl_toolkits.mplot3d import Axes3D
        import matplotlib.pyplot as plt
        import cpk
        fig = plt.figure(facecolor='#ffffff')
        ax = fig.add_subplot(111, projection='3d')
        ax.grid(False)
        ax.xaxis.pane.set_edgecolor('black')
        ax.yaxis.pane.set_edgecolor('black')
        ax.zaxis.pane.set_edgecolor('black')
        ax.xaxis.pane.fill = False
        ax.yaxis.pane.fill = False
        ax.zaxis.pane.fill = False
        dmin, dmax = 0., 0.
        for atoms, cell in zip(self.atoms, self.cells):
            chems = np.array(atoms.get_chemical_symbols())
            for ii, chem in enumerate(set(chems)):
                color = cpk.color(chem)
                size = 10+cpk.size(chem)
                pos = layer.atoms[chem == chems].get_positions()
                pmax = np.max(np.max(pos))
                if dmax < pmax: dmax = pmax
                pmin = np.min(np.min(pos))
                if pmin < dmin: dmin = pmin
                x, y, z = pos[:,0], pos[:,1], pos[:,2]
                style = {'facecolor': color, 's': size, 'alpha':1.0, 'lw':0.2}
                ax.scatter(x, y, z, label='{}'.format(chem), **style)
            ax.legend()
            if show_cell:
                for side in sides(cell):
                    ax.plot(side[:,0]/c.Ang, side[:,1]/c.Ang, zs=side[:,2]/c.Ang, 
                            color='#000000')

        if show_grid:
            x, y, z = self.grid[:,0], self.grid[:,1], self.grid[:,2]
            ax.scatter(x/c.Ang, y/c.Ang, z/c.Ang, marker='.', facecolor='#000000')
        ax.set_xlim([dmin, dmax])
        ax.set_ylim([dmin, dmax])
        ax.set_zlim([dmin, dmax])
        warnings.filterwarnings("ignore")
        fig.tight_layout()
