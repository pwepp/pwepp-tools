#!/usr/bin/env python
import numpy as np
import ase
import ase.io.vasp
import math
#import matplotlib
#matplotlib.use('PDF')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.patches import Rectangle

import seaborn as sns

sns.set_style("white")
sns.set_style("ticks", {'xtick.direction': 'in',
                        'ytick.direction': 'in'})

fig=plt.figure(figsize=(5,5), facecolor='white')
gs = gridspec.GridSpec(2, 2, width_ratios=[100, 15], height_ratios=[100, 15])
gs.update(left=0.1, right=0.99, 
          bottom=0.1, top=0.99,
          wspace=0.0, hspace=0.0)
ax0=fig.add_subplot(gs[0], aspect='equal')
ax1=fig.add_subplot(gs[1], aspect='equal', sharey=ax0)
ax2=fig.add_subplot(gs[2], aspect='equal', sharex=ax0)
plt.setp(ax0.get_xticklabels(), visible=False)
plt.setp(ax1.get_yticklabels(), visible=False)

for poscar, angle, offset, C_color in (('armchair/POSCAR', 0.0, (0.,1.42/2,-1), '#222222'), 
                                       ('zigzag/POSCAR', 90.0, (0.,0., 1), '#222288')):
    atoms = ase.io.vasp.read_vasp(poscar)
    rotation = angle * 2*np.pi/360
    rotation_mat = np.array([[math.cos(rotation), -math.sin(rotation), 0.0],
                             [math.sin(rotation),  math.cos(rotation), 0.0],
                             [0.0               ,  0.0               , 1.0]]);
    chem_symb = np.array(atoms.get_chemical_symbols())
    C = atoms[chem_symb == 'C']
    H = atoms[chem_symb == 'H']
    a = atoms.get_cell()
    absa=np.sqrt(np.sum(a**2,1))

    Cpos = np.dot(absa*(C.get_scaled_positions()-0.5), rotation_mat)
    Hpos = np.dot(absa*(H.get_scaled_positions()-0.5), rotation_mat)
    for ii in range(3):
        Cpos[:,ii] = Cpos[:,ii] + offset[ii]
        Hpos[:,ii] = Hpos[:,ii] + offset[ii]
    absa = np.dot(absa, rotation_mat)

    C_size = 20
    H_size = 6
    H_color = '#dddddd'
    side_alpha = 1.0
    top_alpha = 1.0

    offset = np.array(offset)

    ax0.scatter(Cpos[:,0], Cpos[:,1], facecolor=C_color, s=C_size, alpha=top_alpha, lw=0)
    ax0.scatter(Hpos[:,0], Hpos[:,1], facecolor=H_color, s=H_size, alpha=top_alpha, lw=0.1)
    ax0.add_patch(Rectangle(-absa[[0,1]]/2+offset[[0,1]], absa[0], absa[1], fill=False))

    ax1.scatter(Cpos[:,2], Cpos[:,1], facecolor=C_color, s=C_size, alpha=side_alpha, lw=0)
    ax1.scatter(Hpos[:,2], Hpos[:,1], facecolor=H_color, s=H_size, alpha=side_alpha, lw=0.1)
    ax1.add_patch(Rectangle(-absa[[2,1]]/2+offset[[2,1]], absa[2], absa[1], fill=False))

    ax2.scatter(Cpos[:,0], Cpos[:,2], facecolor=C_color, s=C_size, alpha=side_alpha, lw=0)
    ax2.scatter(Hpos[:,0], Hpos[:,2], facecolor=H_color, s=H_size, alpha=side_alpha, lw=0.1)
    ax2.add_patch(Rectangle(-absa[[0,2]]/2+offset[[0,2]], absa[0], absa[2], fill=False))

ax0.set_xlim([-44,44])
ax0.set_ylim([-46,46])
ax1.set_xlim([-6,6])
ax1.set_xticks([-5, 0, 5])
ax2.set_ylim([-6,6])
ax2.set_yticks([-5, 0, 5])

ax2.set_xlabel('($\\rm \\AA$)')

plt.savefig('structure.pdf')
plt.show()
