#!/usr/bin/env python
'''
Usage: 
    formfactor.py [--plot] [--plot-q2] [--] <Vs> <Va>
'''

import docopt
import numpy as np
import scipy.optimize as opt
import matplotlib.pyplot as plt

from . import constants as c

arguments = docopt.docopt(__doc__)

Vs_in = [float(p) for p in arguments['<Vs>'].split(',')] 
Va_in = [float(p) for p in arguments['<Va>'].split(',')] 

Vs = dict(zip([0,3,8,11],Vs_in))
Va = dict(zip([0,3,4,11],Va_in))

V1, V2 = Vs.copy(), Vs.copy()
for q in Va:
    V1[q] = V1.get(q, 0.0) + Va[q]
    V2[q] = V2.get(q, 0.0) - Va[q]

a_BN = 0.3615 *c.nm
a_C = 0.3567 *c.nm
a_Si = 0.5431 *c.nm

V_BN = 5.905
V_C = 5.672

b2 = (2.*np.pi/a_BN)**2
b2_C = (2.*np.pi/a_C)**2

q1 = np.array(V1.keys()) * b2
q2 = np.array(V2.keys()) * b2
V1q = np.array(V1.values())
V2q = np.array(V2.values())

def Vq_kuro(q2, b0, b1, b2, b3):
    return abs(b0)*(abs(b2)*q2-abs(b1))/(np.exp(abs(b2)*q2-abs(b3))+1.0)

b_kuro_C = (1.781, 1.424, 0.354, 0.938)

def Vsa_fit(b):
    q2 = np.array([0., 3., 4., 8., 11.])*b2
    V1 = Vq_kuro(q2, b[0], b[1], b[2], b[3])
    V2 = Vq_kuro(q2, b[4], b[5], b[6], b[7])
    Vfs = (V1+V2)/2
    Vfa = (V1-V2)/2
    return np.array([
        Vfs[0] - Vs[0],
        Vfa[0] - Va[0],
        Vfs[1] - Vs[3],
        Vfa[1] - Va[3],
        Vfa[2] - Va[4],
        Vfs[3] - Vs[8],
        Vfs[4] - Vs[11],
        Vfa[4] - Va[11]])

b_fit, bla = opt.leastsq(Vsa_fit, [1.781,1.424,0.354,0.938,
                                   1.781,1.424,0.354,0.938])
q_fine = np.linspace(0., 2*11., 1000)

if arguments['--plot-q2']:
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(q_fine/b2, Vq_kuro(q_fine, *b_kuro_C)*V_C/V_BN, color='#333333')
    ax.plot(q_fine/b2, Vq_kuro(q_fine, *b_kuro_C), color='#333333', linestyle='--')
    ax.scatter(q1/b2, V1q, color='#dd3409')
    ax.plot(q_fine/b2, Vq_kuro(q_fine, *b_fit[:4]), color='#dd3409')
    ax.scatter(q2/b2, V2q, color='#0248ee')
    ax.plot(q_fine/b2, Vq_kuro(q_fine, *b_fit[4:]), color='#0248ee')
    plt.axhline(0, color='#000000', linestyle='-')
    plt.axvline(3, color='#000000', linestyle='-')
    plt.axvline(3*b2_C/b2, color='#000000', linestyle='--')
    plt.axvline(4, color='#000000', linestyle='-')
    plt.axvline(4*b2_C/b2, color='#000000', linestyle='--')
    plt.axvline(8, color='#000000', linestyle='-')
    plt.axvline(8*b2_C/b2, color='#000000', linestyle='--')
    plt.axvline(11, color='#000000', linestyle='-')
    plt.axvline(11*b2_C/b2, color='#000000', linestyle='--')
    plt.ylabel('$V(q)$')
    plt.xlabel('$q^2$ in $(2\pi/a_{\\rm BN})^2$')
    plt.xlim([0, 2*11])

if arguments['--plot']:
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(np.sqrt(q_fine/b2), Vq_kuro(q_fine, *b_kuro_C)*V_C/V_BN, color='#333333')
    ax.plot(np.sqrt(q_fine/b2), Vq_kuro(q_fine, *b_kuro_C), color='#333333', linestyle='--')
    ax.scatter(np.sqrt(q1/b2), V1q, color='#dd3409')
    ax.plot(np.sqrt(q_fine/b2), Vq_kuro(q_fine, *b_fit[:4]), color='#dd3409')
    ax.scatter(np.sqrt(q2/b2), V2q, color='#0248ee')
    ax.plot(np.sqrt(q_fine/b2), Vq_kuro(q_fine, *b_fit[4:]), color='#0248ee')
    plt.axhline(0, color='#000000', linestyle='-')
    plt.axvline(np.sqrt(3), color='#000000', linestyle='-')
    plt.axvline(np.sqrt(3*b2_C/b2), color='#000000', linestyle='--')
    plt.axvline(np.sqrt(4), color='#000000', linestyle='-')
    plt.axvline(np.sqrt(4*b2_C/b2), color='#000000', linestyle='--')
    plt.axvline(np.sqrt(8), color='#000000', linestyle='-')
    plt.axvline(np.sqrt(8*b2_C/b2), color='#000000', linestyle='--')
    plt.axvline(np.sqrt(11), color='#000000', linestyle='-')
    plt.axvline(np.sqrt(11*b2_C/b2), color='#000000', linestyle='--')
    plt.ylabel('$V(q)$')
    plt.xlabel('$q$ in $2\pi/a_{\\rm BN}$')
    plt.xlim([0, np.sqrt(2*11)])

print 'B'
print '25'
print '5.905'
print '3'
print 'Kurokawa'
print '{} {} {} {}'.format(*b_fit[:4])
print 'N'
print '25'
print '5.905'
print '5'
print 'Kurokawa'
print '{} {} {} {}'.format(*b_fit[4:])

if arguments['--plot'] or arguments['--plot-q2']:
    plt.show()
