from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

def valence_electrons(atom):
    if atom.number <= 2 : return atom.number
    elif atom.number <= 18: return (atom.number - 2)%8
    elif atom.number <= 54: return (atom.number - 18)%18
    else: return (atom.number - 54)%32
