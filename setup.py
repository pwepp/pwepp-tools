from setuptools import setup
import sys

# make sure nobody ever accidentally publishes this package
def forbid_publish():
    argv = sys.argv
    blacklist = ['register', 'upload']

    for command in blacklist:
        if command in argv:
            values = {'command': command}
            print('Command "%(command)s" has been blacklisted, exiting...' %
                  values)
            sys.exit(2)
forbid_publish()

setup(name='pwepp',
      version='0.1',
      description='Tools for use with the PWEPP simulator',
      url='https://gitlab.com/pwepp/pwepp',
      author='Maarten Van de Put',
      author_email='vandeput.maarten@gmail.com',
      packages=['pwepp'],
      install_requires=[
          'docopt',
          'numpy',
          'scipy',
          'matplotlib',
          'seaborn',
          'ase',
      ],
      scripts=[
          'bin/flake-gui',
          'bin/flakegen',
          'bin/poscar-gen',
          'bin/supercell',
          'bin/bandstructure',
          'bin/waveplot',
          'bin/pwepp-export-bandstructure',
          'bin/pwepp-export-wavefunction',
          'bin/pwepp-meta',
          'bin/pwepp-hash',
          'bin/pwepp-electrons',
          'bin/densityofstates',
          'bin/bandstructure-and-densityofstates'
      ],
      classifiers=[
          'Development Status :: 3 - Alpha',
          'Environment :: Console',
          'Environment :: X11 Applications',
          'Intended Audience :: Science/Research',
          'Natural Language :: English',
          'Operating System :: MacOS :: MacOS X',
          'Operating System :: POSIX :: Linux',
          'Operating System :: POSIX :: AIX',
          'Programming Language :: Python :: 2.7',
          'Topic :: Scientific/Engineering :: Physics',
          'Topic :: Scientific/Engineering :: Visualization'
      ],
      zip_safe=False)
