from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import sys

class Timer(object):
    def __init__(self, identifier):
        self.identifier = identifier

    def __enter__(self):
        self.start = time.time()
        return self

    def __exit__(self, *args):
        self.end = time.time()
        self.secs = self.end - self.start
        self.msecs = self.secs * 1000  # millisecs
        print('timing: {}: {:,.0f} ms'.format(self.identifier, self.msecs), file=sys.stderr)


def graceful_exit(message="Exiting."):
    OriginalExceptHook = sys.excepthook
    exit_msg = "\n{}".format(message)
    def GracefulExceptHook(type, value, traceback):
        if type == KeyboardInterrupt:
            exit(exit_msg)
        else:
            OriginalExceptHook(type, value, traceback)
    sys.excepthook = GracefulExceptHook

def printnow(message):
    print(message)
    sys.stdout.flush()

