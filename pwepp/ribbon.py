from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import numpy.linalg as la
import ase
import ase.io.vasp
from . import constants as c
from . import wavecar
from .utility import fermi_dirac, delta_gauss
from .caching import cached_property

class RibbonError(Exception):
    pass

class Ribbon(wavecar.WAVECAR):
    def __init__(self, folder='./', replicate=None, min_size=None, metadata=True):
        super(Ribbon, self).__init__(folder, metadata)

        if np.any(self.a - np.diag(np.diag(self.a))):
            raise RibbonError('ribbons must have a diagonal basis')
        if replicate is not None and min_size is not None:
            raise ValueError('replicate and min_size cannot both be specified at the same time')

        self.replicate = np.array([1, 1, 1])
        if min_size is not None:
            min_size = np.asarray(min_length)
            absa = super(Ribbon, self).absa
            if min_size.size == 1:
                self.replicate[self.free_idx] = np.ceil(min_size / absa[self.free_idx])
            else:
                self.replicate = np.ceil(min_size / absa)
        if replicate is not None:
            replicate = np.asarray(replicate, dtype=int)
            if replicate.size == 1:
                self.replicate[self.free_idx] = replicate[0]
            else:
                self.replicate = replicate
    
    @property
    def dimensions(self):
        return (self.atoms.positions.max(0) - self.atoms.positions.min(0)) * c.Ang

    @cached_property
    def free_direction(self):
        dk = self.k[1,:] - self.k[0,:]
        return dk / la.norm(dk)

    @property
    def free_idx(self):
        return np.where(self.free_direction != 0.)

    @property
    def free_dk(self):
        return la.norm(np.dot(self.free_direction, self.b))

    @cached_property
    def resolution(self):
        resolution = self.optimal_grid_size()
        free_res = resolution[self.free_idx]
        multiplier = self.absa / self.absa[self.free_idx]
        while np.any(free_res*multiplier < resolution):
            free_res += 1
        return np.ceil(free_res * multiplier)

    @cached_property
    def atoms(self):
        atoms = ase.io.vasp.read_vasp(self.poscar)
        return atoms.repeat(self.replicate)

    @property
    def cell(self):
        cell = np.concatenate(([[0,0,0]], self.atoms.get_cell()*c.Ang))
        return cell

    @property
    def shape(self):
        return self.resolution * self.replicate

    @property
    def big_a(self):
        return self.replicate[:,None] * self.a

    @property
    def big_absa(self):
        return self.replicate * self.absa

    @property
    def grid3(self):
        return self.r(self.resolution, replicate=self.replicate)

    @property
    def grid(self):
        return self.grid3.reshape((3,-1)).T

    @property
    def b_lin(self):
        return la.norm(np.dot(self.free_direction, self.b))

    def dos(self, Es, oversampling=10):
        Nk = oversampling * self.nk
        dE = Es[1]-Es[0]
        dos = np.zeros(Es.shape)
        k, Ek, Erange = self.interpolated_bands
        k_fine = np.linspace(-k[-1], k[-1], Nk, endpoint=False) # full brillouin zone
        dk = k_fine[1] - k_fine[0]
        for iband in range(self.nband):
            for ii, E in enumerate(Es):
                dos[ii] += np.sum(delta_gauss(Ek[iband](abs(k_fine)) - E, dE)) * dk / (2 * np.pi)
        # spin degeneracy
        return 2 * dos

    def charge(self, mu, T=300*c.K, oversampling=5):
        Nk = oversampling * self.nk
        k, Ek, Erange = self.interpolated_bands
        k_fine = np.linspace(-k[-1], k[-1], Nk, endpoint=False) # full brillouin zone
        dk = k_fine[1] - k_fine[0]
        rho = self.nvalence * self.free_dk
        for iband in range(self.nband):
            for ik in k_fine:
                rho -= fermi_dirac(Ek[iband](abs(ik))-mu, T) * dk
        return rho / np.pi

    def doped_fermi(self, charge, T=300*c.K):
        from scipy.optimize import brentq
        def chargeneutral(mu):
            return self.charge(mu, T) - charge
        return brentq(chargeneutral, self.fermi - 1.5*c.eV,
                                     self.lowest_conduction + 1.5*c.eV)

    def realwave(self, ik, iband, operator=None, offset=None, envelope=True):
        return super(Ribbon, self).realwave(ik, iband,
                                            resolution=self.resolution,
                                            envelope=envelope,
                                            operator=operator,
                                            replicate=self.replicate,
                                            offset=offset)

