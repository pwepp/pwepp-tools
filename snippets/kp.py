#!/usr/bin/env python
'''
Usage:
    kp.py <ik> <ibands>
'''
import docopt
import numpy as np
import numpy.linalg as la
import wavecar
import eigenval
import constants as c

def interband_momentum(wc, ik, ibands):
    P = np.zeros((ibands.size, ibands.size, 3), dtype=complex)
    for i0, iband0 in enumerate(ibands):
        G0, psi0 =  wc.wave(ik, iband0)
        Ppsi0 = np.dot(wc.b, G0) * psi0 / (0.1 * c.nm)
        for i1, iband1 in enumerate(ibands):
            G1, psi1 =  wc.wave(ik, iband1)
            for dim in range(3):
                P[i1, i0, dim] = np.vdot(psi1, Ppsi0[dim,:])
    return P

def kp(E, P, k):
    H = np.sum(k**2)/2*np.eye(E.size) + np.dot(P, k) + np.diag(E)
    return la.eigh(H)

if __name__ == '__main__':
    arguments = docopt.docopt(__doc__, version="K.P v0.1")
    ik = int(arguments['<ik>'])
    ibands = np.arange(*(int(iband) for iband in arguments['<ibands>'].split(':')))
    nbands = len(ibands)
    wc = wavecar.WAVECAR()
    ev = eigenval.Eigenval()
    P = interband_momentum(wc, ik, ibands)
    E = ev.E[ik,ibands] * c.eV

    ks = ev.k/c.nm
    nk = ks.shape[0]
    Es = np.zeros((nk, nbands))
    for ii, k in enumerate(ks):
        Es[ii,:], wi = kp(E, P, k-ks[ik])

    import matplotlib.pyplot as plt
    import seaborn as sns
    sns.set_style("white")
    sns.set_style("ticks")
    sns.set_palette("husl")

    fig = plt.figure(facecolor='white')
    ax = fig.add_subplot(111)
    ax.plot(np.r_[0:nk], ev.E, color='#444444', linestyle='--')
    ax.plot(np.r_[0:nk], Es/c.eV)
    fig.tight_layout()
    plt.show()
