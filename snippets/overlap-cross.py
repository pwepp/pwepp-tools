#!/usr/bin/env python
'''
Usage:
    overlap.py band <iband0> <iband1> [--offset=<x>,<y>,<z>] [--reuse]
    overlap.py rotation [--offset=<x>,<y>,<z>] [--reuse]
    overlap.py current [--offset=<x>,<y>,<z>] [--reuse]
    overlap.py -h | --help
    overlap.py --version

Options:
    -h --help
    --version
    --reuse
'''
import sys
import docopt
import numpy as np
import numpy.linalg as la
import scipy.interpolate
import ase
import ase.io.vasp
import glob
import math
from math import pi

import wavecar
import eigenval

import matplotlib.pyplot as plt
import seaborn as sns

sns.set_style("white")
sns.set_style("ticks")

arguments = docopt.docopt(__doc__, version='Overlap 0.1')

Kelvin = 1./3.158e5

def fermi(E, T=300):
    return 1./(1+np.exp(E/(T*Kelvin)))

def current(ii, E0, E1, overlap, Ef0, Ef1):
    J = {}
    for ii0, Ei0 in enumerate(E0):
        for ii1, Ei1 in enumerate(E1):
            Ji = -2*pi*(Ei0-Ei1) * overlap[ii,ii0,ii1] * (fermi(Ei0-Ef0) - fermi(Ei1-Ef1))
            try:
                J[Ei0-Ei1] += Ji
            except KeyError:
                J[Ei0-Ei1] = Ji

    return np.array(J.items())

distance = 2.0 # Angstrom
nvstates = 10
ncstates = 10

cmap=plt.get_cmap('Blues')
directory = 'zigzag'
atoms0 = ase.io.vasp.read_vasp('{}/POSCAR'.format(directory))
wc0 = wavecar.WAVECAR(folder=directory)
ev0 = eigenval.Eigenval(folder=directory)
a0 = atoms0.get_cell()
absa0 = np.sqrt(np.sum(a0**2,1))
cbfirst0 = np.where(ev0.E[0,:] > 0.0)[0][0]
bands0 = cbfirst0 + np.r_[-nvstates:ncstates]
E0 = ev0.E[0,bands0]

directory = 'armchair'
atoms1 = ase.io.vasp.read_vasp('{}/POSCAR'.format(directory))
wc1 = wavecar.WAVECAR(folder=directory)
ev1 = eigenval.Eigenval(folder=directory)
a1 = atoms1.get_cell()
absa1 =np.sqrt(np.sum(a1**2,1))
cbfirst1 = np.where(ev1.E[0,:] > 0.0)[0][0]
bands1 = cbfirst1 + np.r_[-nvstates:ncstates]
E1 = ev1.E[0,bands1]

angles = np.array([90., 91., 92., 93., 94., 95])*2*pi/360
offset = np.array([0.0, 0.0, 2.0])
n = np.array([350, 350, 350])

overlaps = np.zeros((len(angles), len(bands0), len(bands1)))

n_interp = 150

if arguments['--reuse']:
    overlaps = np.load("realoverlaps.npy")
else:
    print "calculating overlaps"
    for ii, angle in enumerate(angles):
        print "  angle: {}".format(360*(angle/2/pi))
        rotation = np.array([[math.cos(angle), -math.sin(angle), 0.0],
                             [math.sin(angle),  math.cos(angle), 0.0],
                             [0.0            ,  0.0            , 1.0]])

        # the zigzag is offset
        x0 = np.linspace(-absa0[0]/2, absa0[0]/2, n[0], endpoint=False)+offset[0]
        y0 = np.linspace(-absa0[1]/2, absa0[1]/2, n[1], endpoint=False)+offset[1]
        z0 = np.linspace(-absa0[2]/2, absa0[2]/2, n[2], endpoint=False)+offset[2]

        # the armchair is rotated
        x1 = np.linspace(-absa1[0]/2, absa1[0]/2, n[0], endpoint=False)
        y1 = np.linspace(-absa1[1]/2, absa1[1]/2, n[1], endpoint=False)
        z1 = np.linspace(-absa1[2]/2, absa1[2]/2, n[2], endpoint=False)
        xyz1 = np.array([x1, y1, z1]).T
        xyz1 = np.dot(xyz1, rotation)
        x1, y1, z1 = xyz1[:,0], xyz1[:,1], xyz1[:,2]

        ipointsx = np.linspace(max(np.min(x0), np.min(x1)), min(np.max(x0), np.max(x1)), n_interp)
        ipointsy = np.linspace(max(np.min(y0), np.min(y1)), min(np.max(y0), np.max(y1)), n_interp)
        ipointsz = np.linspace(max(np.min(z0), np.min(z1)), min(np.max(z0), np.max(z1)), n_interp)
        ones_interp = np.ones(n_interp)
        ipoints = np.array([np.kron(np.kron(ipointsx,    ones_interp), ones_interp),
                            np.kron(np.kron(ones_interp, ipointsy   ), ones_interp),
                            np.kron(np.kron(ones_interp, ones_interp), ipointsz   )]).T
        
        for ii0, band0 in enumerate(bands0):
            G0, wave0 = wc0.wave(0, band0)
            realwave0 = wavecar.reciprocal_to_real(n, G0, wave0)
            irealwave0 = scipy.interpolate.interpn((x0, y0, z0), realwave0, ipoints, 
                                                   bounds_error=False, fill_value=0.0) 
            for ii1, band1 in enumerate(bands1):
                G1, wave1 = wc1.wave(0, band1)
                realwave1 = wavecar.reciprocal_to_real(n, G1, wave1)
                irealwave1 = scipy.interpolate.interpn((x1, y1[::-1], z1), realwave1[:,::-1,:], ipoints,
                                                       bounds_error=False, fill_value=0.0)
                # overlap calculation
                overlaps[ii, ii0, ii1] += np.abs(np.vdot(irealwave0, irealwave1))**2
                print '.',
                sys.stdout.flush()
            print '!',
            sys.stdout.flush()
    np.save("realoverlaps.npy", overlaps)
        
if arguments['rotation']:
    for ii, angle in enumerate(angles):
        fig = plt.figure()
        fig.suptitle('Rotation {}'.format(360*(angle/2/pi)))
        ax = fig.add_subplot(111)
        ms = ax.matshow(overlaps[ii], cmap=cmap)
        fig.colorbar(ms)
    plt.show()

if arguments['band']:
    print range(20)
    print bands0
    print bands1
    ii0 = np.where([bands0 == int(arguments['<iband0>'])] )[1][0]
    ii1 = np.where([bands1 == int(arguments['<iband1>'])] )[1][0]
    print (E0[ii0], E1[ii1])
    fig = plt.figure(figsize=(5,2.5), facecolor='white')
    ax = fig.add_subplot(111)
    lplt = plt.plot(360*angles/2/pi, overlaps[:,ii0,ii1], 'o-')[0]
    plt.ylabel('|overlap|$^2$')
    plt.xticks(360*angles/2/pi, ['{:.0f}$^\circ$'.format(360*angle/2/pi) for angle in angles])
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.subplots_adjust(wspace=0, hspace=0, left=0.15, right=0.97, top=0.94, bottom=0.12)
    plt.savefig('overlap.pdf')
    plt.show()

eV = 0.0367498438131638
Amp = 150.97530044084786
if arguments['current']:
    V = np.linspace(0, 1.5, 1000)*eV
    fig = plt.figure(figsize=(5,5), facecolor='white')
    ax = fig.add_subplot(111)
    for ii, angle in enumerate(angles):
        Ef0 = -0.1*eV
        Ef1 = 0.1*eV
        J = current(ii, E0*eV, E1*eV, overlaps, Ef0, Ef1)
        print E0
        print E1
        idxs = np.argsort(J[:,0])
        J = J[idxs,:]
        Jsmooth = np.zeros(V.shape)
        for Ji in J:
            Jsmooth += Ji[1] * np.exp(-np.abs(V-Ji[0])**2/(300*Kelvin)**2)
        p =ax.plot(V/eV-0.2, Jsmooth/Amp, label='{} deg'.format(int(360*angle/2/pi)))
        ax.scatter(J[:,0]/eV-0.2, J[:,1]/Amp, color=p[0].get_color())
        #ax.scatter(J[:,0]/eV, J[:,1]/Amp, label='{} deg (T=0K)'.format(int(360*angle/2/pi)), color=p[0].get_color())
    ax.legend(loc=2)
    ax.set_xlim([0, 1])
    ax.set_ylim([-0, 6e-7])
    ax.set_xlabel('V (V)')
    ax.set_ylabel('J (A)')
    plt.subplots_adjust(wspace=0, hspace=0, left=0.12, right=0.97, top=0.96, bottom=0.12)
    plt.savefig("current.pdf")
    plt.show()
