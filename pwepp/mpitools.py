from mpi4py import MPI

class MPISession(object):
    def __init__(self):
        self.comm = MPI.COMM_WORLD
        self.size = self.comm.Get_size()
        self.rank = self.comm.Get_rank()

    @property
    def is_root(self):
        return self.rank == 0

    def send(self, data, dest=0, tag=0):
        self.comm.Send(data, dest=dest, tag=tag)

    def receive(self, data, source=0, tag=0):
        self.comm.Recv(data, source=source, tag=tag)
    
    recv = receive

