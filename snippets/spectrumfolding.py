#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

sns.set_style('white')

x = np.linspace(-1., 1., 500)
y = x**2
x0 = 1.1 * x**0 
y0 = -.1 * y**0

xv = 0.1 + np.r_[0:6]*.17
xc = -.24 - np.r_[0:6]*.13
yv = xv**2
yc = xc**2

xv0 = 1.1 * xv**0
yv0 = -.1 * yv**0
xc0 = 1.1 * xc**0
yc0 = -.1 * yc**0

fig = plt.figure(facecolor='white', figsize=(5,5))
ax = fig.add_subplot(111)
ax.plot(x, y, 'k-')
ax.plot(x0, y, 'k-')
ax.plot(x, y0, 'k-')
for x, y in zip(xv, yv):
    ax.plot([x, x, 1.1], [-.1, y, y], color='#3333dd')
for x, y in zip(xc, yc):
    ax.plot([x, x, 1.1], [-.1, y, y], color='#dd3333')
ax.plot([0, 0], [-.1, 1.], linestyle='--', color='#555555')
ax.scatter(xv, yv, color='#3333dd')
ax.scatter(xc, yc, color='#dd3333')
ax.scatter(xv0, yv, color='#3333dd')
ax.scatter(xc0, yc, color='#dd3333')
ax.scatter(xv, yv0, color='#3333dd')
ax.scatter(xc, yc0, color='#dd3333')
ax.set_xticks([0])
ax.set_xticklabels(['$\sigma$'])
ax.set_yticks([0])
ax.set_yticklabels(['$0$'])
ax.set_xlabel("Spectrum $\\bf H$")
ax.set_ylabel("Spectrum $({\\bf H} - \sigma)^2$")
ax.set_xlim([-1,1.15])
ax.set_ylim([-.15,1])
sns.despine(fig=fig, ax=ax, top=True, left=True, right=True, bottom=True)
ax.yaxis.set_label_position('right')
plt.savefig('spectral_folding.pdf')
plt.show()
