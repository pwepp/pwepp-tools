from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import math
import numpy as np
import numpy.linalg as la

def rotation_matrix(axis, angle):
    '''Rotate by [angle] radians around [axis]'''
    l,m,n = axis / la.norm(axis)
    ca, sa = math.cos(angle), math.sin(angle)
    alpha = 1 - ca
    return np.array([
                [l*l*alpha + ca, m*l*alpha - n*sa, n*l*alpha + m*sa],
                [l*m*alpha + n*sa, m*m*alpha + ca, n*m*alpha - l*sa],
                [l*n*alpha - m*sa, m*n*alpha - l*sa, n*n*alpha + ca]
            ])

def reflection_matrix(axis):
    '''Reflection through plane perpendicular to [axis] and through (0,0,0)'''
    a,b,c = axis / la.norm(axis)
    return np.array([
                [1-2*a**2, -2*a*b, -2*a*c],
                [-2*a*b, 1-2*b**2, -2*b*c],
                [-2*a*c, -2*b*c, 1-2*c**2]
            ])

def rotate(data, axis, angle, zero=None):
    '''rotates data over an angle around an axis placed at zero'''
    axis = np.asarray(axis)
    if zero is not None:
        data = translate(data, -zero)
    try:
        rdata = np.dot(rotation_matrix(axis, angle), data)
    except ValueError:
        rdata = np.dot(rotation_matrix(axis, angle), data.T).T
    if zero is None:
        return rdata
    else:
        return translate(data, zero)[0]

def reflect(data, axis, zero=None):
    '''reflects data through a plane intersecting a given axis placed at zero'''
    axis = np.asarray(axis)
    if zero is not None:
        data = translate(data, -zero)
    try:
        rdata = np.dot(reflection_matrix(axis), data)
    except ValueError:
        rdata = np.dot(reflection_matrix(axis), data.T).T
    if zero is None:
        return rdata
    else:
        return translate(data, zero)[0]

def translate(data, vector):
    vector = np.asarray(vector)
    '''translates data along a vector'''
    try:
        return data + vector[None,:]
    except ValueError:
        return data + vector[:,None]

def center(data):
    '''finds the center of data'''
    c = np.average(data, 1 if data.shape[0] == 3 else 0)
    return c

class Position(object):
    '''positions data with a predefined set of transformations'''
    def __init__(self, data=None):
        self.operations = []
        self.operations_repr = []

    def __str__(self):
        return '{}: {}'.format(self.__class__.__name__, 
                                 ', '.join(self.operations_repr))

    def rotate(self, axis, angle, zero=None):
        self.operations_repr.append('rotated')
        self.operations.append(lambda data: rotate(data, axis, angle, zero=zero))
        return self

    def reflect(self, axis, zero=None):
        self.operations_repr.append('reflected')
        self.operations.append(lambda data: reflect(data, axis, zero=zero))
        return self

    def translate(self, vector):
        self.operations_repr.append('translated')
        self.operations.append(lambda data: translate(data, vector))
        return self

    def center(self, data):
        self.operations_repr.append('centered')
        for op in self.operations:
            data = op(data)
        c = center(data)
        self.operations.append(lambda data: translate(data, -c))
        return self

    def __call__(self, data):
        for f in self.operations:
            data = f(data)
        return data

def alignment_offset(self, data, refdata):
    from scipy import optimize as opt
    from . import constants as c
    def total_offset(offset):
        tdata = translate(data, offset)
        offsets = np.sqrt(np.sum((tdata[:,None,:] - refdata[None,:,:])**2, 2))
        offsets = np.min(offsets,0)
        return np.sum(offsets**2)
    return opt.basinhopping(total_offset, np.array([0.,0.,0.])).x

