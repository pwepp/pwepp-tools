#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

sns.set_style('white')
sns.set_style('ticks')
sns.set_palette("husl")

def T(q):
    return q**2/2

def kresse(q, T_R):
    x = 2.*T(q)/(3*T_R)
    return 3.*T_R/4 * (27 + 18*x + 12*x**2 + 8*x**3 + 16*x**4) / (27 + 18*x + 12*x**2 + 8*x**3)

def sfold(q, T_R, sigma):
    return (kresse(q, T_R) - sigma)**2

if __name__ == '__main__':
    q = np.linspace(0., 10., 100)

    fig = plt.figure(facecolor='white')
    ax = fig.add_subplot(111)
    for T_R in 2.*np.r_[1:6]:
        ax.plot(q, kresse(q, T_R), label='$T(|R\\rangle) = {}$'.format(T_R))
    ax.plot(q, T(q), color='#eeeeee', linestyle='--')
    ax.set_xlabel('$|q|$')
    ax.set_ylabel('$\\langle q|H-\\epsilon I|q \\rangle$')
    ax.legend(loc='upper left')
    fig.set_tight_layout(True)

    fig = plt.figure(facecolor='white')
    ax = fig.add_subplot(111)
    for T_R in 2.*np.r_[1:6]:
        ax.plot(q, sfold(q, T_R, 5.), label='$T(|R\\rangle) = {}$'.format(T_R))
    ax.plot(q, T(q), color='#eeeeee', linestyle='--')
    ax.set_xlabel('$|q|$')
    ax.set_ylabel('$\\langle q|(H-\\sigma I)^2-\\epsilon I|q \\rangle$')
    ax.legend(loc='upper left')
    fig.set_tight_layout(True)

    plt.show()


