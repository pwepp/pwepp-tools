from math import sqrt

special_numbers = [
        (sqrt(2),     r'\sqrt{2}'),
        (1./sqrt(2),  r'\frac{1}{\sqrt{2}}'),
        (sqrt(3),     r'\sqrt{3}'),
        (1./sqrt(3),  r'\frac{1}{\sqrt{3}}'),
        (sqrt(3)/2,   r'\frac{\sqrt{3}}{2}'),
        (2/sqrt(3),   r'\frac{2}{\sqrt{3}}'),
        (1./2,        r'\frac{1}{2}'),
        (1./3,        r'\frac{1}{3}'),
        (2./3,        r'\frac{2}{3}'),
        (3./8,        r'\frac{3}{8}'),
        (1./4,        r'\frac{1}{4}'),
        (2./4,        r'\frac{2}{4}'),
        (3./4,        r'\frac{3}{4}'),
        (1.,          r'1'),
        (0.,          r'0'),
        (-sqrt(2),    r'\bar\sqrt{2}'),
        (-1./sqrt(2), r'\bar\frac{1}{\sqrt{2}}'),
        (-sqrt(3),    r'\bar\sqrt{3}'),
        (-1./sqrt(3), r'\bar\frac{1}{\sqrt{3}}'),
        (-sqrt(3)/2,  r'\bar\frac{\sqrt{3}}{2}'),
        (-2/sqrt(3),  r'\bar\frac{2}{\sqrt{3}}'),
        (-0.5,        r'\bar\frac{1}{2}'),
        (-1./3,       r'\bar\frac{1}{3}'),
        (-1.,         r'\bar 1')
    ]

def float_to_special(value):
    for test, string in special_numbers:
        if round(abs(value-test), 5) == 0:
            return string
    return '{:.2f}'.format(value).rstrip('0').rstrip('.')

def floats_to_specials(values):
    return map(float_to_special, values)
