'''Physical constants and units in Hartree atomic units
ref: CODATA 2010, NIST, http://physics.nist.gov/cuu/Constants/index.html
'''
from __future__ import division
from math import pi

# mathematical constant
degree = pi/180

## fixed physical constants
q_e = 1
m_e = 1
hbar = 1
k_B = 1
eps0 = 0.25 / pi

# charge
C = 1.0 / 1.602176565e-19

# current
A = 1.0 / 6.62361795e-3

# mass
kg = 1.0 / 9.10938291e-31 # kilogram

# energy
Ry = 0.5            # Rydbergs
eV = 3.674932379e-2 # electron-volt
J = 1.0 / 4.35974434e-18  # joule

# temperature
K = 8.6173324e-5 * eV # kelvin

# force
N = 1.0 / 8.23872278e-8 # newton

# length
m = 1.0 / 0.52917721092e-10 # meter
cm = 1e-2 * m               # centimeter
um = 1e-6 * m               # micrometer
micron = um                 # micrometer
nm = 1e-9 * m               # nanometer
Ang = 1e-10 * m             # Angstrom

# volume
m3 = m**3   # cube meter
cm3 = cm**3 # cube centimeter

# magnetic flux
T = 1.0 / 2.350517464e5 # tesla

# time
s = 1.0 / 2.418884326502e-17 # second
ps = 1e-12 * s               # femtosecond
fs = 1e-15 * s               # femtosecond
