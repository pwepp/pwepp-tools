from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os.path as path
from struct import unpack, pack
import numpy as np
import numpy.linalg as la
import numpy.fft as fft
import scipy.ndimage.interpolation as ndi
import ase
import ase.io.vasp
from . import constants as c
from . import asetools
from . import grids
from .caching import cached_property

class WavecarError(Exception):
    pass

class WAVECAR(object):
    def __init__(self, folder='.', metadata=True):
        folderparts = folder.split('/')
        if 'WAVECAR' in folderparts[-1]:
            self.folder = path.expanduser('/'.join(folderparts[:-1]))
            self.file = path.expanduser(folder)
        else:
            self.folder = path.expanduser(folder)
            self.file = '{}/WAVECAR'.format(path.expanduser(folder))
        self.poscar = '{}/POSCAR'.format(self.folder)
        if metadata:
            try:
                self.load_metadata()
            except IOError:
                pass

    def __str__(self):
        return '{} from \'{}\''.format(self.__class__.__name__, self.folder)

    def __repr__(self):
        return '<{}({})>'.format(self.__class__.__name__, self.folder)

    @cached_property
    def endian(self):
        '''Endianness used in the WAVECAR: '<' (small endian) | '>' (big endian)'''
        with open(self.file, 'rb') as f:
            line = f.read(8)
        nrecl_big = unpack('>d', line)[0]
        nrecl_little = unpack('<d', line)[0]
        if int(nrecl_big) - nrecl_big == 0.0:
            return '>'
        elif int(nrecl_little) - nrecl_little == 0.0:
            return '<'
        elif nrecl_big > 3:
            return '>'
        elif nrecl_little > 3:
            return '<'
        else:
            return None

    @cached_property
    def reclen(self):
        '''Record length used in the WAVECAR'''
        with open(self.file, 'rb') as f:
            return int(unpack(self.endian+'d', f.read(8))[0])

    @cached_property
    def spin(self):
        with open(self.file, 'rb') as f:
            f.seek(8)
            return int(unpack(self.endian+'d', f.read(8))[0])

    @cached_property
    def precision(self):
        '''Numerical precision used in the WAVECAR: 'single' | 'double' '''
        with open(self.file, 'rb') as f:
            f.seek(2*8)
            prec = int(unpack(self.endian+'d', f.read(8))[0])
        return 'single' if prec == 45200 else 'double'

    def _read_sizes(self):
        with open(self.file, 'rb') as f:
            f.seek(self.reclen)
            (nk, nband, Ecut) = unpack(self.endian+'d'*3,f.read(8*3))
        return nk, nband, Ecut*c.eV

    @cached_property
    def nk(self):
        '''Number of calculated k points'''
        return int(self._read_sizes()[0])

    @cached_property
    def nband(self):
        '''Number of calculated bands'''
        return int(self._read_sizes()[1])

    @cached_property
    def Ecut(self):
        '''Cutoff energy'''
        return self._read_sizes()[2]

    @cached_property
    def a(self):
        '''Lattice basis vectors'''
        with open(self.file, 'rb') as f:
            f.seek(self.reclen)
            f.read(8*3)
            a1 = np.array(unpack(self.endian+'d'*3, f.read(8*3)))
            a2 = np.array(unpack(self.endian+'d'*3, f.read(8*3)))
            a3 = np.array(unpack(self.endian+'d'*3, f.read(8*3)))
        return np.array([a1, a2, a3]) * c.Ang

    @cached_property
    def absa(self):
        return np.sqrt(np.sum(self.a**2, 1))

    @cached_property
    def Vcell(self):
        return la.det(self.a)

    @cached_property
    def b(self):
        '''Reciprocal lattice basis vectors'''
        return 2*np.pi * la.inv(self.a.T)

    @cached_property
    def iG(self):
        '''Indices of the G vectors __only use on cutoff wavefunctions__

        Wavefunctions are stored in a wavecar with x as fastest changing index (Fortran style)
            x: - - - - - - - -
            y: --- --- --- ---
            z: ------- -------
        instead of z as fastest changing index (C style).

        Convertion to full representation in Python (C style) accounts for this by
        building the index matrix z-first, i.e. z as slow changing index.
        '''
        # note that wavefunctions are stored as z,y,x instead of x,y,z
        bmag = np.sqrt(np.sum(self.b**2,1))
        Gcut = np.sqrt(2*self.Ecut)
        nbmax = 2 * np.array(Gcut/bmag, dtype=int)
        ig0 = np.mod(np.arange(2*nbmax[0])+nbmax[0], 2*nbmax[0]) - nbmax[0]
        ig1 = np.mod(np.arange(2*nbmax[1])+nbmax[1], 2*nbmax[1]) - nbmax[1]
        ig2 = np.mod(np.arange(2*nbmax[2])+nbmax[2], 2*nbmax[2]) - nbmax[2]
        '''Implementation note:
        - we build the indices z-first (slow index) -> (ig2, ig1, ig0)
        - we reorder such that the output is x-first -> (2,1,0)
        the output is x-first but also has x as the fast index
        '''
        return np.reshape(np.meshgrid(ig2, ig1, ig0, indexing='ij'), (3,-1))[(2,1,0),:]

    @cached_property
    def nG(self):
        '''Total number of G vectors (i.e. natural FFT grid)'''
        return 2*(np.max(self.iG, 1) + 1)

    @cached_property
    def G(self):
        '''G vectors __only use on cutoff wavefunctions__'''
        return np.dot(self.b.T, self.iG)

    def Gmask(self, ik):
        '''Mask is True if G vector is within the cutoff'''
        return self.T(ik) <= self.Ecut

    def nGcut(self, ik):
        '''The number of G vectors within the cutoff, as calculated'''
        Gmask = self.Gmask(ik)
        return np.count_nonzero(Gmask)

    def T(self, ik):
        '''Kinetic energy __only use on cutoff wavefunctions__'''
        k = self.k[ik, :, None]
        return np.sum((self.G + k)**2, 0)/2

    @cached_property
    def kb(self):
        '''k in reciprocal lattice basis. '''
        kb = np.empty((self.nk, 3), dtype=float)
        with open(self.file, 'rb') as f:
            for ik in range(self.nk):
                f.seek(((self.nband+1)*ik+2) * self.reclen + 8)
                kb[ik, :] = np.array(unpack(self.endian+'d'*3, f.read(8*3)))
        return kb

    @cached_property
    def k(self):
        '''k in carthesian coordinates. '''
        return np.dot(self.kb, self.b)

    @cached_property
    def E(self):
        '''Eigenenergies (ik, iband)'''
        E = np.empty((self.nk, self.nband), dtype=float)
        with open(self.file, 'rb') as f:
            for ik in range(self.nk):
                f.seek(((self.nband+1)*ik+2) * self.reclen + 8*4)
                data = np.array(unpack(self.endian+'d'*3*self.nband, f.read(8*3*self.nband)))
                E[ik,:] = data[::3]*c.eV
        return E

    def ascii_E(self):
        kkn, bb = np.meshgrid(np.r_[0:self.nk], np.r_[0:self.nband], indexing='ij')
        kn = np.ravel(kkn)
        b = np.ravel(bb)
        kkx, bb = np.meshgrid(self.k[:,0], np.r_[0:self.nband], indexing='ij')
        kx = np.ravel(kkx)*c.nm
        kky, bb = np.meshgrid(self.k[:,1], np.r_[0:self.nband], indexing='ij')
        ky = np.ravel(kky)*c.nm
        kkz, bb = np.meshgrid(self.k[:,2], np.r_[0:self.nband], indexing='ij')
        kz = np.ravel(kkz)*c.nm
        E = (np.ravel(self.E) - self.fermi)/c.eV
        dEdkx = np.ravel(self.dEdk[:,:,0])/c.eV/c.nm
        dEdky = np.ravel(self.dEdk[:,:,1])/c.eV/c.nm
        dEdkz = np.ravel(self.dEdk[:,:,2])/c.eV/c.nm
        return "\n".join(["{:d} {:d} {:.18e} {:.18e} {:.18e} {:.18e} {:.18e} {:.18e} {:.18e}"
                            .format(ikn, ib, ikx, iky, ikz, iE, idEdkx, idEdky, idEdkz) 
                            for ikn, ib, ikx, iky, ikz, iE, idEdkx, idEdky, idEdkz 
                            in zip(kn, b, kx, ky, kz, E, dEdkx, dEdky, dEdkz)])
    
    def ascii_realwave(self, ik, iband):
        wave = self.realwave(ik, iband)
        r = self.r(wave.shape)
        wave = np.ravel(wave)
        x = np.ravel(r[0,:,:,:])/c.nm
        y = np.ravel(r[1,:,:,:])/c.nm
        z = np.ravel(r[2,:,:,:])/c.nm
        return ascii_fmt(x,y,z,wave)
    
    def ascii_mixedwave(self, ik, iband, mix):
        kaxes = [i for i, x in enumerate(mix) if x == 'k']
        wave = self.mixedwave(ik, iband, mix)
        r = self.r(wave.shape)
        G = self.Gfull
        mask = np.ones(np.prod(wave.shape), dtype=bool)
        Gmask = self.Gmask(ik)
        for kax in kaxes:
            iGf = np.ravel(self.iGfull[kax, :, :, :])
            extent = self.iG[kax, Gmask]
            mask[iGf < extent.min()] = False
            mask[iGf > extent.max()] = False
        x = np.ravel(r[0,:,:,:]/c.nm if (mix[0]=='r') else G[0,:,:,:]*c.nm)[mask]
        y = np.ravel(r[1,:,:,:]/c.nm if (mix[1]=='r') else G[1,:,:,:]*c.nm)[mask]
        z = np.ravel(r[2,:,:,:]/c.nm if (mix[2]=='r') else G[2,:,:,:]*c.nm)[mask]
        wave = np.ravel(wave)[mask]
        return ascii_fmt(x,y,z,wave)

    def ascii_wave(self, ik, iband):
        mask = self.Gmask(ik)
        wave = self.wave(ik, iband)
        Gx = self.G[0,mask]*c.nm        
        Gy = self.G[1,mask]*c.nm        
        Gz = self.G[2,mask]*c.nm      
        return ascii_fmt(Gx,Gy,Gz,wave)

    @cached_property
    def nvalence(self):
        '''Number of valence bands'''
        atoms = ase.io.vasp.read_vasp(self.poscar)
        total_el = sum(asetools.valence_electrons(atom) for atom in atoms)
        return int(total_el / 2)

    @property
    def ivalence(self):
        '''Index of highest energy valence band'''
        return self.nvalence - 1

    @property
    def iconduction(self):
        '''Index of lowest energy conduction band'''
        return self.nvalence

    @property
    def gap(self):
        gap = (self.lowest_conduction - self.fermi)
        return 0. if gap < 0 else gap

    @property
    def midgap(self):
        return self.fermi + self.gap/2

    @cached_property
    def fermi(self):
        '''Fermi energy, maximal energy of highest valence band'''
        return np.max(self.E[:, self.ivalence])

    @cached_property
    def lowest_conduction(self):
        '''Minimal energy of lowest energy conduction band'''
        return np.min(self.E[:, self.iconduction])

    @cached_property
    def occupation(self):
        '''Occupation of the states as given in the wavecar'''
        occ = np.empty((self.nk, self.nband), dtype=float)
        with open(self.file, 'rb') as f:
            for ik in range(self.nk):
                f.seek(((self.nband+1)*ik+2) * self.reclen + 8*4)
                data = np.array(unpack(self.endian+'d'*3*self.nband, f.read(8*3*self.nband)))
                occ[ik, :] = data
        return occ

    @cached_property
    def dEdk(self):
        '''Derivative of energy to k using Hellmann-Feynman'''
        dEdk = np.empty((self.nk, self.nband, 3), dtype=float)
        for ik in range(self.nk):
            for iband in range(self.nband):
                wave = self.wave(ik, iband)
                Gplusk = self.G[:, self.Gmask(ik)] + self.k[ik, :, None]
                Gk_wave = Gplusk * wave
                dEdk[ik, iband, :] = np.real(np.dot(np.conjugate(wave), Gk_wave.T))
        return dEdk

    def wave(self, ik, iband, full=False):
        '''Wavefunction as stored in WAVECAR

        Indexing is z-first (see documentation on iG)
        '''
        with open(self.file, 'rb') as f:
            f.seek(((self.nband+1)*ik+2) * self.reclen)
            nG = int(np.array(unpack(self.endian+'d', f.read(8))))
            f.seek(((self.nband+1)*ik+2+iband+1) * self.reclen)
            wave = np.array(unpack(self.endian+'f'*2*nG, f.read(4*2*nG)))
        wave = wave[::2] + 1j * wave[1::2]
        nGcut = self.nGcut(ik)
        if not (wave.size == nGcut or wave.size == nGcut*2):
            raise WavecarError(
                    'predicted number of G-vectors of |{},{}> is {} but WAVECAR has {}'
                    .format(ik, iband, nGcut, wave.size))
        if wave.size == 2 * nGcut:
            wave.shape = (2,-1)
        return wave

    def optimal_grid_size(self, n=None):
        if n is None:
            n = self.nG
        if any(np.array(n) < 0):
            n = np.array([-ni*nGi if ni<0 else ni for ni, nGi in zip(n, self.nG)])
        if any(np.array(n) < self.nG):
            print("warning: n is too small, got {} where at least {} is needed"
                    .format(n, self.nG))
            n = np.array([max(ni, nGi) for ni, nGi in zip(n, self.nG)])
        return np.array(n)

    def realwave(self, ik, iband, resolution=None, operator=None,
                 envelope=True, offset=None, replicate=None):
        '''Wavefunction psi(r) or basis function u(r) if envelope is False'''
        if operator not in (None, 'T', 'kinetic'):
            raise NotImplementedError('Operator {} is not implemented'.format(operator))
        resolution = self.optimal_grid_size(resolution)
        wave = self.wave(ik, iband)
        mask = self.Gmask(ik)
        iG = self.iG[:, mask]
        if offset is not None:
            G = self.G[:, mask]
            wave *= np.exp(1j*np.dot(offset ,G))
        if operator == 'kinetic' or operator == 'T':
            wave *= self.T(ik)[mask]
        realwave = np.zeros(resolution, dtype=complex)
        realwave[iG[0,:], iG[1,:], iG[2,:]] = wave
        realwave = fft.ifftn(realwave)
        realwave *= np.prod(self.nG) / np.sqrt(np.prod(resolution))

        if replicate is not None:
            realwave = np.tile(realwave, replicate)
        if envelope:
            realwave *= self.envelope(ik, resolution, offset, replicate)
        return realwave

    def mixedwave(self, ik, iband, mix='rrr'):
        '''Wavefunction in a mixed real (r) / reciprocal (k) basis'''
        resolution = self.optimal_grid_size()
        wave = self.wave(ik, iband)
        mask = self.Gmask(ik)
        iG = self.iG[:, mask]
        mixwave = np.zeros(resolution, dtype=complex)
        mixwave[iG[0,:], iG[1,:], iG[2,:]] = wave
        raxes = [i for i, x in enumerate(mix) if x == 'r']
        if raxes:
            mixwave = fft.ifftn(mixwave, axes=raxes)
            mixwave *= np.prod(self.nG[raxes]) / np.sqrt(np.prod(resolution[raxes]))
        return mixwave

    @property
    def iGfull(self):
        iGf = np.empty((3,) + tuple(self.nG))
        iG = self.iG
        iGf[0,iG[0,:], iG[1,:], iG[2,:]] = iG[0,:]
        iGf[1,iG[0,:], iG[1,:], iG[2,:]] = iG[1,:]
        iGf[2,iG[0,:], iG[1,:], iG[2,:]] = iG[2,:]
        return iGf

    @property
    def Gfull(self):
        G = np.empty((3,) + tuple(self.nG))
        iG = self.iG
        G[0,iG[0,:], iG[1,:], iG[2,:]] = self.G[0,:]
        G[1,iG[0,:], iG[1,:], iG[2,:]] = self.G[1,:]
        G[2,iG[0,:], iG[1,:], iG[2,:]] = self.G[2,:]
        return G

    def r(self, resolution, offset=None, replicate=None):
        offset = np.array([0., 0., 0.] if offset is None else offset)
        replicate = np.array([1, 1, 1] if replicate is None else replicate)
        a = self.a * replicate[:,None]
        shape = resolution * replicate
        return grids.carthesian_grid(a, shape) + offset[:, None, None, None]

    def envelope(self, ik, resolution, offset=None, replicate=None):
        '''Envelope function f(r)=exp(ik.r)'''
        r = self.r(resolution, offset, replicate)
        return np.exp(1j * np.tensordot(self.k[ik], r, [0,0]))
    

    def sort_bands_new(self):
        def fullwave(ik, iband):
            iG = self.iG[:, self.Gmask(ik)]
            wave = self.wave(ik, iband)
            wave_full = np.zeros(self.nG, dtype=complex)
            wave_full[iG[0,:], iG[1,:], iG[2,:]] = wave
            return wave_full

        band_idx = np.empty((self.nk, self.nband), dtype=int)
        band_idx[0, :] = range(self.nband)
        for ik in range(1, self.nk):
            dk = self.k[ik] - self.k[ik-1]
            overlaps = []
            for iband in range(self.nband):
                wave_ref = fullwave(ik-1, band_idx[ik-1, iband])
                dEdk_ref = self.dEdk[ik-1, band_idx[ik-1, iband]]
                E_ref = self.E[ik-1, band_idx[ik-1, iband]] + np.dot(dEdk_ref, dk)
                overlaps.append( [(iband, 1. - np.vdot(wave_ref, fullwave(ik, iband2)))
                        for iband2 in range(self.nband) 
                        if abs(self.E[ik, iband2] - E_ref) < 1*c.eV] )
                overlaps[iband].sort(key=lambda o: o[1])
            guess = [overlap[0][0] for overlap in overlaps]
            duplicates = set([idx for idx in guess if guess.count(idx) > 1])
            if len(duplicates) > 0:
                print("Duplicates found!")
#                for duplicate in duplicates:
#                    positions = [pos for pos, idx in enumerate(guess) if idx == duplicate]        
            band_idx[ik, :] = guess
        return band_idx

    def matel(self, ik0, iband0, ik1, iband1):
        integrand = np.zeros(self.nG, dtype=complex)
        iG0 = self.iG[:, self.Gmask(ik0)]
        iG1 = self.iG[:, self.Gmask(ik1)]
        integrand[iG0[0,:], iG0[1,:], iG0[2,:]] = self.wave(ik0, iband0)
        integrand[iG1[0,:], iG1[1,:], iG1[2,:]] *= np.conj(self.wave(ik1, iband1))
        return np.sum(integrand)

    def kk(self, iband):
        return np.array([[self.matel(iband,i,iband+1,j) for i in range(self.nk)] for j in range(self.nk)])

    @cached_property
    def sort_bands(self):
        return np.arange(self.nk)[:,None], self.sort_bands_new()
        '''Indices for sorting the bands to be continuous with changing k'''
        available = np.ones((self.nk, self.nband), dtype=bool)
        band_idx = np.empty((self.nk, self.nband), dtype=int)
        wave_full0 = np.zeros(self.nG, dtype=complex)
        wave_full1 = np.zeros(self.nG, dtype=complex)
        for iband in range(0, self.nband):
            band_idx[0, iband] = iband
            for ik in range(1, self.nk):
                dE = self.E[ik, available[ik,:]] - self.E[ik-1, band_idx[ik-1, iband]]
                dk = self.k[ik] - self.k[ik-1]
                dE0 = np.sum(self.dEdk[ik-1, band_idx[ik-1,iband],:]*dk)
                overlaps = []
                iG0 = self.iG[:, self.Gmask(ik-1)]
                wave0 = self.wave(ik-1, band_idx[ik-1,iband])
                wave_full0[:,:,:] = 0.0
                wave_full0[iG0[0,:], iG0[1,:], iG0[2,:]] = wave0
                for iband2 in np.flatnonzero(available[ik,:]):
                    iG1 = self.iG[:, self.Gmask(ik)]
                    wave1 = self.wave(ik, iband2)
                    wave_full1[:,:,:] = 0.0
                    wave_full1[iG1[0,:], iG1[1,:], iG1[2,:]] = wave1
                    overlaps.append(np.vdot(wave_full0, wave_full1))
                overlaps = np.abs(np.array(overlaps))
                accuracy = np.abs(dE - dE0)
                if np.argmin(accuracy) == np.argmax(overlaps):
                    match = np.flatnonzero(available[ik,:])[np.argmin(accuracy)]
                else:
                    if np.max(overlaps) > 0.9:
                        match = np.flatnonzero(available[ik,:])[np.argmax(overlaps)]
                    else:
                        match = np.flatnonzero(available[ik,:])[np.argmin(accuracy)]
                available[ik, match] = False
                band_idx[ik, iband] = match
        return np.arange(self.nk)[:,None], band_idx

    @cached_property
    def interpolated_bands(self):
        import scipy.interpolate as interp
        E = self.E[self.sort_bands]
        dk = self.k[1]-self.k[0]
        k = np.arange(self.nk) * la.norm(dk)
        dEdk = np.sum(self.dEdk * dk[None,None,:], axis=2)[self.sort_bands] / la.norm(dk)
        E_range = np.empty((self.nband, 2), dtype=float)
        polylines = []
        for iband in range(self.nband):
            E_dEdk = np.vstack((E[:,iband], dEdk[:,iband])).T
            bpoly = interp.BPoly.from_derivatives(k, E_dEdk)
            ppoly = interp.PPoly.from_bernstein_basis(bpoly)
            E_extrema = ppoly(ppoly.derivative(1).roots(extrapolate=False))
            E_extrema = np.append(E_extrema, (E[0, iband], E[-1, iband]))
            E_range[iband, 0] = np.min(E_extrema)
            E_range[iband, 1] = np.max(E_extrema)
            polylines.append(ppoly)
        return k, polylines, E_range

    def generate_metadata(self, filename='metadata'):
        from .systools import printnow
        printnow('generating dE/dk...',)
        dEdk = self.dEdk
        printnow('done.')
        printnow('generating band sorting...',)
        sort_k, sort_bands = self.sort_bands
        printnow('done.')
        np.savez(filename, dEdk=dEdk,
                           sort_k=sort_k, sort_bands=sort_bands)

    def load_metadata(self, filename='metadata'):
        store = np.load('{}/{}'.format(self.folder,
            filename if filename.endswith('.npz') else filename+'.npz'))
        self.dEdk = store['dEdk']
        self.sort_bands = store['sort_k'], store['sort_bands']

def replicate(a, wave, k, N):
    a = a * np.array(N)[:,None]
    # k.r = k.(a^T.ri) = (k.a^T).ri
    ri = np.mgrid[0:N[0], 0:N[1], 0:N[2]]
    ka = np.dot(k, a.T)
    phases = np.exp(1j*np.tensordot(ka, ri, [0,0]))
    return a, np.kron(phases, wave)

def minimize_interpolation(shape, points):
    shape = np.array(shape)[:, None]
    di = np.array(points * shape) - np.array(np.round(points * shape), dtype=int)
    return np.average(di, 1) / shape[:,0]

def interpolate(wave, points, method='map'):
    ''' interpolates the wave on the given points

    points: the coordinates of the points in the cell as a fraction of the lattice vectors
    method: 'nearest' is fastest, default uses map_coordinates

    '''
    if method == 'nearest':
        shape = np.array(wave.shape)[:, None]
        idx = np.array(np.round(points * shape) % shape, dtype=int)
        return wave[idx[0,:], idx[1,:], idx[2,:]]

    else:
        idx = points * (np.array(wave.shape)[:, None])
        rw = ndi.map_coordinates(np.real(wave), idx, mode='wrap')
        iw = ndi.map_coordinates(np.imag(wave), idx, mode='wrap')
        return rw + 1j * iw

def ascii_fmt(x, y, z, wave):
    return "\n".join(["{:.18e} {:.18e} {:.18e} {:.18e} {:.18e}"
                       .format(ix, iy, iz, iwaver, iwavei) 
                        for ix, iy, iz, iwaver, iwavei 
                        in zip(x, y, z, np.real(wave), np.imag(wave))])
