#!/usr/bin/env python
import sys
from struct import unpack, pack 
from numpy import *
from numpy.linalg import det,inv
order='C'
#try:
#    from fftw import fft3 as fftn
#    order='F'
#except:
from numpy.fft import fftn, ifftn
import read_xml
#from read_pot import get_potdim
end='<'

def guess_endian(folder='.'):
    if not 'WAVECAR' in folder: folder=folder+'/WAVECAR'
    f = open(folder, 'rb')
    line = f.read(8)
    f.close()
    nrecl_big = unpack('>d', line)[0]
    nrecl_little = unpack('<d', line)[0]
    if int(nrecl_big) - nrecl_big == 0:
        return '>'
    elif int(nrecl_little) - nrecl_little == 0:
        return '<'
    else:
        return None

def get_b(folder='.',return_a=False):
    if not 'WAVECAR' in folder: folder=folder+'/WAVECAR'
    f = open(folder, 'rb') 
    (nrecl,nspin,nprec)=[int(xi) for xi in unpack(end+'d'*3,f.read(8*3))]
    if nspin>1:
        error('Spin not defined')
    f.seek(nrecl)
    (xnwk,xnband,ecut)=unpack(end+'d'*3,f.read(8*3))
    a1=array(unpack(end+'d'*3,f.read(8*3)))
    a2=array(unpack(end+'d'*3,f.read(8*3)))
    a3=array(unpack(end+'d'*3,f.read(8*3)))
    nwk=int(xnwk)
    nband=int(xnband)
    a=array([a1,a2,a3])
    Vcell=det(a)
    b=2.*pi*inv(a.T)
    bmag=sqrt(sum(b**2,1))
    b_norm=b/bmag
    f.close()
    if return_a:
        return a
    else:
        return b,nwk,nband,ecut,nrecl,Vcell

def get_nk(folder='.'):
    b,nwk,nband,ecut,nrecl,Vcell=get_b(folder)
    return nwk

def get_nband(folder='.'):
    b,nwk,nband,ecut,nrecl,Vcell=get_b(folder)
    return nband

c=0.262465831
def get_g(b,ecut):
    bmag=sqrt(sum(b**2,1))
    nbmax= 2*array(sqrt(ecut*c)/bmag,dtype=int)#+1

    gx=mod(arange(2*nbmax[0])+nbmax[0],2*nbmax[0])-nbmax[0]
    gy=mod(arange(2*nbmax[1])+nbmax[1],2*nbmax[1])-nbmax[1]
    gz=mod(arange(2*nbmax[2])+nbmax[2],2*nbmax[2])-nbmax[2]
    
#    gx=arange(2*nbmax[0])-nbmax[0]
#    gy=arange(2*nbmax[1])-nbmax[1]
#    gz=arange(2*nbmax[2])-nbmax[2]

    g=reshape(meshgrid(gx,gy,gz,indexing='ij'),(3,-1))[[0,1,2],:]
    return g

def get_wave(ik,iband,folder='.'):
    b,nk,nband,ecut,nrecl,Vcell=get_b(folder)
    if iband<0:
        iband=mod(iband,nband)

    g=get_g(b,ecut)
    if not 'WAVECAR' in folder: folder=folder+'/WAVECAR'
    f = open(folder, 'rb') 
    f.seek(((nband+1)*ik+2)*nrecl)
    nplane=int(array(unpack(end+'d',f.read(8))))
    wk=array(unpack(end+'d'*3,f.read(8*3)))
    eo=array(unpack(end+'d'*2*nband,f.read(8*2*nband)))
    #print('k',wk)
    #print('E',eo)

    wave=[]

    f.seek(((nband+1)*ik+2+iband+1)*nrecl)
    wave=array(unpack(end+'f'*2*nplane,f.read(4*2*nplane)))
    wave=wave[::2]+1j*wave[1::2]

    Ig=sum(dot(b.T,g+reshape(wk,(-1,1)))**2,0)/c<ecut
    sg=count_nonzero(Ig)
    #sg=sum(Ig)
    if (size(wave)==sg or size(wave)==sg*2):
        g=g[:,Ig]
        #g=g[:,where(Ig)[0]]
        #g=g[:,sort(argsort(sum(dot(b.T,g+reshape(wk,(-1,1)))**2,0))[:sg])]
    else:
        print('WARNING: NOT AUTOMATICALLY CORRECT NUMBER of GS',size(wave),sg)
        if size(wave)-1.5*sg>0:
            sg=size(wave)/2
        else:
            sg=size(wave)
        g=g[:,sort(argsort(sum(dot(b.T,g+reshape(wk,(-1,1)))**2,0))[:sg])]

    if size(wave)==2*size(g,1):
        wave.shape=(2,-1)
    f.close()
    return wave,g,Vcell

def wave_real(n=None,g=0,wave=0,folder='.',offset=None):
    if not any(n):
        n=read_xml.get_ngx(folder)
    elif n=='F':
        n=read_xml.get_ngx(folder,n)
    elif n=='P':
        n=get_potdim(folder+'/LOCPOT')
    if size(g)==1:
        if wave<0:
            waves=[]
            for iband in range(get_nband()):
                waves.append(wave_real(n,g,iband,folder))
            return array(waves)
        else:
            wave,g,Vcell=get_wave(g,wave,folder)
    if not offset==None:
        g+=reshape(array(offset),(-1,1))
    if wave.ndim==2:
        wave_r=zeros((2,n[0],n[1],n[2]),dtype=complex,order=order)
        for (iwave, wavei) in enumerate(wave):
            wave_r[iwave]=wave_real(n,g,wavei,folder,None)
    else:
        wave_r=zeros(n,dtype=complex,order=order)
        wave_r[g[0,:], g[1,:], g[2,:]]=wave

        if not order=='F':
            wave_r=ifftn(wave_r)
        else:
            ifftn(wave_r)

        wave_r*=sqrt(prod(n))

    return wave_r

def print_siminfo(n,ik,iband):
    #return
    wave_r=wave_real(n,ik,iband)
    wave_r2=wave_real(n,ik,iband+1)
    print('norm',sum(wave_r*conj(wave_r)))
    print('norm',sum(wave_r*conj(transpose(wave_r[::-1],(0,2,1,3)))))
    print('norm',sum(wave_r2*conj(transpose(wave_r[::-1],(0,2,1,3)))))
    matrix=array([[sum(wave_r*conj(transpose(wave_r[::-1],(0,2,1,3)))),
                        sum(wave_r*conj(transpose(wave_r2[::-1],(0,2,1,3))))],
                  [sum(wave_r2*conj(transpose(wave_r[::-1],(0,2,1,3)))),
                        sum(wave_r2*conj(transpose(wave_r2[::-1],(0,2,1,3))))]])/(
                        sum(abs(wave_r)**2))
    print(matrix)
    print(eig(matrix)[0])
    print('inversion symm',trace(matrix))
    matrix2=array([[sum(wave_r*wave_r[::-1]),
                        sum(wave_r*wave_r2[::-1])],
                  [sum(wave_r2*wave_r[::-1]),
                        sum(wave_r2*wave_r2[::-1])]])/(
                        sum(abs(wave_r)**2))
    print(matrix2)
    print('time rev sym',trace(matrix2))
    for i in range(0*33):
        wave_r2=wave_real(n,ik,i)
        print('norm '+str(i),abs(sum(wave_r2*conj(transpose(wave_r,(0,2,1,3))))))
    exit()


if __name__=="__main__":

    from pylab import *
    from mpl_toolkits.mplot3d import Axes3D

    import argparse
    #from pymatgen.io.vaspio import Poscar
    
    parser = argparse.ArgumentParser(description='Plot potential')
    parser.add_argument('axis',nargs='*',default=[0])
    parser.add_argument('--bigendian', nargs='?', default=False,const=True)
    parser.add_argument('--ik',nargs='?',default=0,type=int)
    parser.add_argument('--iband',nargs='?',default=0,type=int)
    parser.add_argument('--nbands',nargs='?',default=1,type=int)
    parser.add_argument('--n',nargs='?',default=None)
    parser.add_argument('--surf',nargs='?',default=False,const=True)
    parser.add_argument('--plot',nargs='?',default=False,const=True)
    parser.add_argument('--pdf',nargs='?',default=False,const=True)
    parser.add_argument('--eps',nargs='?',default=False,const=True)
    parser.add_argument('--spinflip',nargs='?',default=False,const=True)
    parser.add_argument('--dot',nargs='?',default=False,const=True)
    parser.add_argument('--fontsize',nargs='?',default=18,type=int)
    parser.add_argument('--figsize',nargs='?',default=1.5,type=float)
    parser.add_argument('--folder',nargs='?',default='.')
    parser.add_argument('--folder2',nargs='?',default=None)
    args=parser.parse_args()
    args.axis=tuple(array(args.axis,dtype=int))
    if size(args.axis)==1:
        args.axis=args.axis[0]

    end = guess_endian(folder='.');
    if not end:
        print('could not obtain endianness')
        exit(-1)
#    if args.bigendian: end = '>'

    n=100*ones(3,dtype=int)#read_xml.get_ngx('.')

    guess_endian(args.folder)
    for iiband, iband in enumerate(range(args.iband,args.iband+args.nbands)):
        wave,g,Vcell=get_wave(args.ik,iband,args.folder)
        if args.folder2!=None:
            wave2,g,Vcell=get_wave(args.ik,iband,args.folder2)
            print(sum(abs(wave)**2),sum(abs(wave2)**2))
            wave=wave/wave2
    
        wave_r=wave_real(n,g,wave)
        #print_siminfo(n,args.ik,iband)
        abswave_r=abs(wave_r)**2
        if args.dot:
            print('dot',sum(abs(wave_r)**2),abs(sum(wave_r[::-1]*wave_real(n,args.ik,iband+1)))**2)
            wave_r=wave_r*wave_real(n,args.ik,iband+1)[::-1]
            abswave_r=real(wave_r)
    
        noaxis=arange(3)[arange(3) != args.axis]
        y=abs(wave_r)**2
        if wave_r.ndim==3:
            fig=figure(figsize=(10,5))
            ax=fig.gca()
            if size(args.axis)==1:
                z=sum(y,args.axis)/size(y,args.axis)
                a=get_b(args.folder,return_a=True)#a=read_xml.get_a()
                a[a==0.]=1.
                absa=sqrt(sum(a**2,1))
                #absa=abs(diag(a))
                nx,ny=size(z,0),size(z,1)
                xx=arange(0.+nx)/nx
                yy=arange(0.+ny)/ny
                #print(absa)
                x,y=meshgrid(absa[noaxis[0]]*xx,absa[noaxis[1]]*yy,indexing='ij')
                if size(z,1)<size(z,0):
                    x,y=y.T,x.T
                    z=z.T
                #ax.contour(x,y,z,amin(z)+(amax(z)-amin(z))*linspace(0.02,1., 50))
                p1=ax.contourf(x,y,z,amin(z)+(amax(z)-amin(z))*linspace(0.02,1., 50))
                ax.set_aspect('equal','box-forced')
                fig.colorbar(p1)
#                contour(z,amin(z)+(amax(z)-amin(z))*linspace(0.02,1,30),colors='k')
#                contourf(z,amin(z)+(amax(z)-amin(z))*linspace(0.02,1,30))
                show()
            else:
                print(sum(abs(wave_r)**2))
                plot(abs(wave_r[0,0,:])**2/Vcell)
                show()
                contour(sum(abs(wave_r)**2,2))
                show()
        else:
            print('Spin orbit activated')
            font = { 'size'   : args.fontsize}
   
            if args.surf:
    
                rc('font', **font)
                fig=figure(figsize=(10,5))
                ax=fig.gca(projection='3d')
                z=sum(abs(wave_r)**2,(0,3))
    
                x,y=meshgrid(range(size(z,1)),range(size(z,0)))
                ax.plot_surface(x,y,z,cmap=cm.jet,rstride=1,cstride=1)
                show()
            elif args.plot:
                z=sum(abs(wave_r)**2,(0,1,3))
                plot(z)
                show()
            else:
                if iiband==0:
                    if args.pdf or args.eps:
                        f, axarr = plt.subplots(2,1, sharex=True,figsize=[2/1.5*args.figsize,args.figsize])
                        font= {'family': 'serif', 'serif': ['Computer Modern'],'size':10}
                        rc('font', **font)
                        rc('text', usetex=True)
                    else:
                        rc('font', **font)
                        f, axarr = plt.subplots(2,1, sharex=True)
                        axarr[0].tick_params(pad=15)
                        axarr[1].tick_params(pad=15)
                        f.subplots_adjust(hspace=.001)
                    a=read_xml.get_a()
                    absa=sqrt(sum(a**2,1))
                    absa=abs(diag(a))
                    axarr[1].set_xlabel('$y$ (${\\rm \AA}$)')
                    axarr[1].set_ylabel('$z$ (${\\rm \AA}$)')
                    axarr[0].set_ylabel('$z$ (${\\rm \AA}$)')
                    #axarr[0].axis([-absa[1]/2.,absa[1]/2.,-round(absa[2]/10.)*5,round(absa[2]/10.)*5])
                    #axarr[1].axis([-absa[1]/2.,absa[1]/2.,-round(absa[2]/10.)*5,round(absa[2]/10.)*5])
                    #axarr[0].set_yticks(arange(-round(absa[2]/10.)*5.,absa[2]/2., 5))
                    #axarr[1].set_yticks(arange(-round(absa[2]/10.)*5.,absa[2]/2., 5))
                    #axarr[1].set_yticklabels(arange(0,int(absa[2]-5),10))
                    #axarr[1].set_xticks(arange(0,,10))
                    axarr[0].text(0.05,1-.03,'$\uparrow$',horizontalalignment='left',verticalalignment='top',transform=axarr[0].transAxes)
                    axarr[1].text(0.05,1-.03,'$\downarrow$',horizontalalignment='left',verticalalignment='top',transform=axarr[1].transAxes)
                    try:
                        poscar=Poscar.from_file('POSCAR')
                        coords=array(poscar.structure.frac_coords)
                        center=sum(coords,0)/size(coords,0)
                        print(center)
                    except:
                        center=zeros(3)
    
    
    #            plot(abs(wave_r[:,0,0,:].T)**2/Vcell)
    #            show()
    #            contour(sum(abs(wave_r[0])**2,2))
    #            show()
    #            contour(sum(abs(wave_r[1])**2,2))
    #            show()
                if size(args.axis)==1:
                    print(args.axis)
                    z0=abswave_r
                    if args.spinflip:
                        z0=z0[::-1]
                    z1=sum(z0,args.axis+1)/size(z0,args.axis+1)
                    
                    for ispin in range(2):
                        z=sum(z0[ispin],args.axis)/size(z0[ispin],args.axis)
                        nx,ny=size(z,0),size(z,1)
                        xx=arange(0.+nx)/nx
                        yy=arange(0.+ny)/ny
                        I=where(xx>.5+center[noaxis[0]])[0][0]
                        xx=xx-mean(xx)
                        yy=yy-mean(yy)
                        #z=z[mod(I+arange(nx),nx),:]
                        x,y=meshgrid(absa[noaxis[1]]*yy,absa[noaxis[0]]*xx)
                        if size(z,1)<size(z,0):
                            x,y=y.T,x.T
                            z=z.T
                        #axarr[ispin].contour(z,amin(z)+(amax(z)-amin(z))*linspace(0,1,30),colors='k')
                        if args.pdf or args.eps:
                            axarr[ispin].set_position(axarr[ispin].get_position().bounds*array([1.,1.,2.,1.]))
                        axarr[ispin].contour(x,y,z,amin(z)+(amax(z1)-amin(z1))*linspace(0.02,1., 50))
                        axarr[ispin].set_aspect('equal','box-forced')
                        #axarr[ispin].set_yticks(arange(0,30,10))
                    #yticks(arange(0,20,10))
                #f.subplots_adjust(hspace=-.19)
                #f.subplots_adjust(hspace=-.45)
    if args.pdf:
        savefig("plot_wave.pdf",format="pdf",bbox_inches='tight')
    elif args.eps:
        savefig("plot_wave.ps",format="eps",bbox_inches='tight')
    else:
        show()
