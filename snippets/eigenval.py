#!/usr/bin/env python
import numpy as np
import ase
import ase.io.vasp
def valence_electrons(atom):
    if atom.number <= 2 : return atom.number
    elif atom.number <= 18: return (atom.number - 2)%8
    elif atom.number <= 54: return (atom.number - 18)%18
    else: return (atom.number - 54)%32

class Eigenval(object):
    def __init__(self, folder='./'):
        if 'eigenval.dat' in folder.split('/')[-1]:
            self.file = folder
            self.folder = '/'.join(folder.split('/')[:-1])
        else:
            self.file = folder+'/eigenval.dat' 
            self.folder = folder
        self.data = np.loadtxt(self.file, ndmin=2)
        self.nk = self.data.shape[0]
        self.ik = self.data[:,0]
        self.k = self.data[:,1:4]
        self.E = self.data[:,4:]

    @property
    def ivalence(self):
        try:
            self._ivalence
        except AttributeError:
            atoms = ase.io.vasp.read_vasp(self.folder+'/POSCAR')
            total_el = np.sum(valence_electrons(atom) for atom in atoms)
            self._ivalence = total_el / 2 - 1
        return self._ivalence

    @property
    def iconduction(self):
        return self.ivalence + 1

    @property
    def fermi(self):
        try:
            self._fermi
        except AttributeError:
            self._fermi =  np.max(self.E[:, self.ivalence])
        return self._fermi

    @property
    def lowest_conduction(self):
        try:
            self._lowest_conduction
        except AttributeError:
            self._lowest_conduction = np.min(self.E[:, self.iconduction])
        return self._lowest_conduction

