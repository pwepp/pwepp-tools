from __future__ import print_function
import sys
import time
import numpy as np

def fermi_dirac(dE, T=0.):
    if T==0.0:
        return 1. * (dE <= 0)
    else:
        dET = dE/T
        return 1.*(dET < -80) + (-80 < dET) * (dET < 80) * 1./(1 + np.exp(dET))

def delta_sinc(E, dE):
    return dE/np.pi * np.sin(np.pi*E/dE)**2 / E**2

def delta_rect(E, dE):
    return dE**-1 * (np.abs(E) <= dE/2)

def delta_gauss(E, dE):
    return np.exp(-E**2/(2*dE**2)) / (dE * np.sqrt(2*np.pi))

def cdiff(A):
    cdA = np.zeros(A.shape)
    dA = np.diff(A)
    cdA[:-1] = dA/2
    cdA[1:] += dA/2
    cdA[0] *= 2
    cdA[-1] *= 2
    return cdA

def checksum(filename):
    from hashlib import md5
    hasher = md5()
    with open(filename,'rb') as f:
        for i, chunk in enumerate(iter(lambda: f.read(8192), b'')):
            hasher.update(chunk)
            if i > 100: break
    return hasher.hexdigest()

def hex2rgb(value):
    value = value.lstrip('#')
    lv = len(value)
    return tuple(float(int(value[i:i + lv//3], 16))/256 for i in range(0, lv, lv//3))
