from os import path

def xyz_to_012(string):
    return {'x':0, 'y': 1, 'z': 2}[string]

def scaled(t, factor):
    def inner(value):
        return t(value) * factor
    return inner

def filepath(value):
    return path.expandvars(path.expanduser(value))

def contractuser(value):
    return value.replace(path.expanduser('~'), '~')

