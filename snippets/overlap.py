#!/usr/bin/env python
'''
Usage:
    overlap.py band [--offset=<x>,<y>,<z>] [--reuse] [--realspace]
    overlap.py rotation [--offset=<x>,<y>,<z>] [--reuse] [--realspace]
    overlap.py energy [--offset=<x>,<y>,<z>] [--reuse] [--together] [--realspace]
    overlap.py current [--offset=<x>,<y>,<z>] [--reuse] [--realspace]
    overlap.py -h | --help
    overlap.py --version

Options:
    -h --help
    --version
    --reuse
'''

import docopt
import numpy as np
import numpy.linalg as la
import scipy.interpolate
import ase
import ase.io.vasp
import glob
import math
from math import pi

import wavecar
import eigenval

import matplotlib.pyplot as plt
import seaborn as sns

sns.set_style("white")
sns.set_style("ticks")

arguments = docopt.docopt(__doc__, version='Overlap 0.1')

Kelvin = 1./3.158e5

def fermi(E, T=300):
    return 1./(1+np.exp(E/(T*Kelvin)))

def current(ii, E, overlap, Ef0, Ef1):
    J = {}
    for ii0, Ei0 in enumerate(E):
        for ii1, Ei1 in enumerate(E):
            Ji = -2*pi*(Ei0-Ei1) * overlap[ii,ii0,ii1] * (fermi(Ei0-Ef0) - fermi(Ei1-Ef1))
            try:
                J[Ei0-Ei1] += Ji
            except KeyError:
                J[Ei0-Ei1] = Ji

    return np.array(J.items())

distance = 2.0 # Angstrom
nvstates = 10
ncstates = 10
#dirs = ["0", "5", "10", "15", "20", "25", "30"]
dirs = ["zigzag", "armchair"]

cmap=plt.get_cmap('Blues')
directory = dirs[0]
atoms0 = ase.io.vasp.read_vasp('{}/POSCAR'.format(directory))
wc0 = wavecar.WAVECAR(folder=directory)
ev0 = eigenval.Eigenval(folder=directory)

E_round = np.round(np.squeeze(ev0.E[0,:]), decimals=50)
idx = np.where(E_round > 0.0)[0][0]
print(idx)
Es = np.unique(E_round[idx-nvstates:idx+ncstates])
print Es
bands = [{'ibands': np.where(E_round == Ei)[0],
          'Es': ev0.E[0, E_round==Ei],
          'E': Ei} for Ei in Es]

a = atoms0.get_cell()
absa=np.sqrt(np.sum(a**2,1))

overlaps = np.zeros((len(dirs), len(bands), len(bands)))#, dtype=complex)
angle = pi/2
rotation = np.array([[math.cos(angle), -math.sin(angle)],
                     [math.sin(angle),  math.cos(angle)]])

offset = np.array([0.0, 0.0, 2.0])
if arguments['--offset']:
    offset = np.array([float(v) for v in arguments['--offset'].split[',']])

if arguments['--reuse']:
    if arguments['--realspace']:
        overlaps = np.load("realoverlaps.npy")
    else:
        overlaps = np.load("overlaps.npy")
else:
    print "calculating overlaps"
    if arguments['--realspace']:
        n = np.array([300, 300, 300])
        length0 = np.sqrt(np.sum(atoms0.get_cell()**2,1))
        x0 = np.linspace(-length0[0]/2,length0[0]/2, n[0], endpoint=False)
        y0 = np.linspace(-length0[1]/2,length0[1]/2, n[1], endpoint=False)
        xy0 = np.array([x0, y0]).T
        xy0 = np.dot(xy0, rotation)
        x0 = xy0[:,0]
        y0 = xy0[:,0]
        z0 = np.linspace(-length0[2]/2,length0[2]/2, n[2], endpoint=False)
        for ii, directory in enumerate(dirs):
            atoms1 = ase.io.vasp.read_vasp('{}/POSCAR'.format(directory))
            length1 = np.sqrt(np.sum(atoms1.get_cell()**2,1))
            x1 = np.linspace(-length1[0]/2,length1[0]/2, n[0], endpoint=False)
            y1 = np.linspace(-length1[1]/2,length1[1]/2, n[1], endpoint=False) + 1.42 / 2
            z1 = np.linspace(-length1[2]/2,length1[2]/2, n[2], endpoint=False)
            ipointsx = np.linspace(max(np.min(x0), np.min(x1)),min(np.max(x0), np.max(x1)), 150)

            ipointsy = np.linspace(max(np.min(y0), np.min(y1)),min(np.max(y0), np.max(y1)), 150)
            ipointsz = np.linspace(max(np.min(z0), np.min(z1)),min(np.max(z0), np.max(z1)), 150)
            ipoints = np.array([np.kron(ipointsx, np.ones(150*150)),
                                np.kron(np.kron(np.ones(150), ipointsy), np.ones(150)),
                                np.kron(np.ones(150*150), ipointsz)]).T
            print x1.shape
            print "  directory '{}'".format(directory)
            atoms1 = ase.io.vasp.read_vasp('{}/POSCAR'.format(directory))
            wc1 = wavecar.WAVECAR(folder=directory)
            for ii0, band0 in enumerate(bands):
                print "    states {}".format(band0['ibands'])
                for iband0 in band0['ibands']:
                    G0, wave0 = wc0.wave(0, iband0)
                    wave0 = wave0 * np.exp(1j * np.dot(offset/2, G0))
                    realwave0 = wavecar.reciprocal_to_real(n, G0, wave0)
                    irealwave0 = scipy.interpolate.interpn((x0, y0, z0), realwave0, ipoints, bounds_error=False, fill_value=0.0) 
                    for ii1, band1 in enumerate(bands):
                        for iband1 in band1['ibands']:
                            G1, wave1 = wc1.wave(0, iband1)
                            wave1 = wave1 * np.exp(1j * np.dot(-offset/2, G1))
                            realwave1 = wavecar.reciprocal_to_real(n, G1, wave1)
                            irealwave1 = scipy.interpolate.interpn((x1, y1, z1), realwave1, ipoints, bounds_error=False, fill_value=0.0) 
                            overlaps[ii, ii0, ii1] += np.abs(np.vdot(irealwave0, irealwave1))**2
        np.save("realoverlaps.npy", overlaps)
        
    else:
        for ii, directory in enumerate(dirs):
            print "  directory '{}'".format(directory)
            atoms1 = ase.io.vasp.read_vasp('{}/POSCAR'.format(directory))
            wc1 = wavecar.WAVECAR(folder=directory)
            for ii0, band0 in enumerate(bands):
                print "    states {}".format(band0['ibands'])
                for iband0 in band0['ibands']:
                    G0, wave0 = wc0.wave(0, iband0)
                    wave0 = wave0 * np.exp(1j * np.dot(offset, G0)) # add some z-offset
                    for ii1, band1 in enumerate(bands):
                        for iband1 in band1['ibands']:
                            G1, wave1 = wc1.wave(0, iband1)
                            overlaps[ii, ii0, ii1] += np.abs(np.vdot(wave0, wave1))**2
        np.save("overlaps.npy", overlaps)

if arguments['rotation']:
    for ii, directory in enumerate(dirs):
        fig = plt.figure()
        fig.suptitle('Rotation {}'.format(directory))
        ax = fig.add_subplot(111)
        ms = ax.matshow(overlaps[ii], cmap=cmap)
        fig.colorbar(ms)
    plt.show()

if arguments['band']:
    maxi = np.max(overlaps)
    fig = plt.figure(facecolor='white')
    for ii0, band0 in enumerate(bands):
        for ii1, band1 in enumerate(bands):
            ax = fig.add_subplot(len(bands), len(bands), len(bands)*ii0 + ii1 + 1)
            ms = ax.plot(range(len(dirs)), overlaps[:,ii0, ii1])
            ax.set_ylim([0, maxi])
            if ii0 == 0:
                ax.set_title("E={:.2f}".format(band1['E']))
            if not (ii0 == len(bands) - 1):
                plt.setp(ax.get_xticklabels(), visible=False)
            if ii1 == 0:
                ax.set_ylabel("E={:.2f}".format(band0['E']))
            else:
                plt.setp(ax.get_yticklabels(), visible=False)
    plt.subplots_adjust(wspace=0, hspace=0, left=0.07, right=0.99, top=0.95, bottom=0.05)
    plt.show()

eV = 0.0367498438131638
Amp = 150.97530044084786
if arguments['current']:
    for ii, directory in enumerate(dirs):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        E = [band['E']*eV for band in bands[:-1]]
        Ef0 = -0.1*eV
        Ef1 = 0.1*eV
        J = current(ii, E, overlaps, Ef0, Ef1)
        idxs = np.argsort(J[:,0])
        ax.plot(J[idxs,0]/eV, J[idxs,1]/Amp, 'o-', label='{} deg'.format(directory))
        ax.set_xlabel('V')
        ax.set_ylabel('A')
        ax.legend()
    plt.show()

if arguments['energy']:
    if arguments['--together']:
        import itertools
        marker = itertools.cycle(('o', 's', 'D', 'v', '^'))
        sns.set_palette(sns.color_palette("coolwarm", len(dirs)), len(dirs))
    
    ii0 = [ii for ii, band in enumerate(bands) if band['E']>0.0][0]
    E0 = bands[ii0]['E']

    if arguments['--together']:
        fig = plt.figure(facecolor='white')
        ax = fig.add_subplot(111)
        plt.hold(True)
        fig.suptitle('Overlap of layer 1, 1st CB with layer 2 states')
        for ii, directory in enumerate(dirs):
            V_olap = np.array([[band['E']-E0, overlaps[ii, ii0, ii1]]
                                    for ii1, band in enumerate(bands)])
            ms = ax.plot(V_olap[:,0], V_olap[:,1], marker=marker.next(), linestyle='-', 
                         label='{} deg'.format(directory))
        ax.set_xlabel('delta E in eV')
        ax.set_ylabel('overlap per state')
        ax.legend()
    if not arguments['--together']:
        for ii, directory in enumerate(dirs):
            fig = plt.figure(facecolor='white')
            ax = fig.add_subplot(111)
            fig.suptitle('Overlap of layer 1, 1st CB with layer 2 states @ {} deg'.format(directory))
            V_olap = np.array([[band['E']-E0, overlaps[ii, ii0, ii1]]
                                    for ii1, band in enumerate(bands)])
            ms = ax.plot(V_olap[:,0], V_olap[:,1], 'o-')
            ax.set_xlabel('delta E in eV')
            ax.set_ylabel('overlap in per state')

    plt.show()
