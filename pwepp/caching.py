class cached_property(object):
    ''' A cached property
    returns value in cache or generate one using the provided method
    "del" clears the cache
    setting a value changes the cache value to that
    '''

    def __init__(self, method):
        self.method = method
        self.__doc__ = method.__doc__
        self.__name__ = method.__name__
        self.__module__ = method.__module__

    def fget(self, instance):
        try:
            return instance._cache[self.__name__]
        except (AttributeError, KeyError):
            value = self.method(instance)
            self.__set__(instance, value)
        return value

    def fset(self, instance, value):
        try:
            cache = instance._cache
        except AttributeError:
            cache = instance._cache = {}
        instance._cache[self.__name__] = value

    def __get__(self, instance, owner):
        return self.fget(instance)

    def __delete__(self, instance):
        try:
            del instance._cache[self.__name__]
        except (AttributeError, KeyError):
            pass
        
    def __set__(self, instance, value):
        self.fset(instance, value)


