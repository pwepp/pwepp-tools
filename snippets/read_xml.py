#!/usr/bin/python
import xml.etree.ElementTree as ET
import sys
from numpy import *
from numpy.linalg import solve
import yaml
import cPickle
import marshal
import read_WAVECAR as rW
import os.path

def try_yaml_load(file):
    filepickle=file[:-5]+'.pickle'
    if os.path.isfile(filepickle) and os.path.getmtime(filepickle) >= os.path.getmtime(file):
        return cPickle.load(open(filepickle,'rb'))
    else:
        try:
            data=yaml.load(open(file),Loader=yaml.CLoader)
        except:
            data=yaml.load(open(file))
        cPickle.dump(data,open(filepickle,'wb'),-1)
        return data

def get_b(folder='.'):
    tree=ET.parse(folder+'/vasprun.xml')
    root = tree.getroot()
    b=[]
    for bi in root.find('structure').find('crystal').find("varray[@name='rec_basis']").findall('v'):
        b.append([float(s) for s in bi.text.split()])
    b=2*pi*array(b)
    return b

def get_a(folder='.'):
    tree=ET.parse(folder+'/vasprun.xml')
    root = tree.getroot()
    a=[]
    for ai in root.find('structure').find('crystal').find("varray[@name='basis']").findall('v'):
        a.append([float(s) for s in ai.text.split()])
    a=array(a)
    return a

def get_ngx(folder,F=None):
    tree=ET.parse(folder+'/vasprun.xml')
    root = tree.getroot()
    root2=root.find('parameters')
    if not F=='F':
        ngx=int(root2.find("separator/i[@name='NGX']").text)
        ngy=int(root2.find("separator/i[@name='NGY']").text)
        ngz=int(root2.find("separator/i[@name='NGZ']").text)
    else:
        ngx=int(root2.find("separator/i[@name='NGXF']").text)
        ngy=int(root2.find("separator/i[@name='NGYF']").text)
        ngz=int(root2.find("separator/i[@name='NGZF']").text)
    return ngx,ngy,ngz

def sort_1D(folder,E):
    if os.path.isfile('WAVECAR_order.npy') and os.path.getmtime('WAVECAR_order.npy') > os.path.getmtime('WAVECAR'):
        I=load('WAVECAR_order.npy')
        for (i, II) in enumerate(I):
            E[i]=E[i][II]
    else:
        n=get_ngx(folder)
        nband=rW.get_nband(folder)
        nk=size(E,0)
        Ik=range(nk)

        I=outer(ones(nk,dtype=int),range(nband))
        x11=rW.wave_real(n,Ik[0],-1)
        x11.shape=(size(x11,0),-1)
        x11=x11/reshape(sqrt(sum(abs(x11)**2,1)),(-1,1))
        Iold=array(range(nband))
        II2=argsort(Iold)
        for ik in Ik[1:]:
            III=array(range(nband))
            I[ik]=Iold.copy()
            E[ik]=E[ik][Iold]
            x2=x11.copy()
            x11=rW.wave_real(n,ik,-1)
            x11.shape=(size(x11,0),-1)
            x11=x11/reshape(sqrt(sum(abs(x11)**2,1)),(-1,1))
            x3=x2
#            D1=E[ik]
#            D2=E[ik-1]
#            h1=dot(x11.conj(),x2.T)
#            H1=dot(h1.conj().T,dot(diag(D1),h1))
#            h2=dot(x2.conj(),x2.T)
#            #print(sum(abs(h2-eye(size(h2,1)))**2))
#            #H2=dot(D2*h2.conj().T,h2)
#            H2=dot(h2.conj().T,dot(diag(D2),h2))
#            #print(sum(abs(H2-diag(D2))**2))
#            #H2=diag(D2)
#            x4=(H1-H2)
#            DD=reshape(D2,(-1,1))-D2
#            DD[DD!=0.]=1/DD[DD!=0.]
#            x5=x4*DD
#            x3+=dot(x5.T,x2)
#            x3=x3/reshape(sqrt(sum(abs(x3)**2,1)),(-1,1))

            M=-abs(sum(x11.conj()*x3,1))**2
            for i2 in range(nband):
                HD=0.
                #x1=x11[I[ik][II2[III]]]
                #x3=x2
                #HD= -sum(abs(sum(x1.conj()*x3,1))**2)
                HD= -sum(abs(sum(x11[I[ik][II2[III]]].conj()*x3,1))**2)
                if i2==0:
                    HD=sum(M)
                    HD0=HD
                else:
                    #M3=-abs(sum(x1.conj()*x2,1))**2
                    Ichange=I[ik][II2[i2-1:i2+1]]
#                    M2=-abs(sum(x11[Ichange[::-1]].conj()*x3[Ichange],1))**2
#                    HD2=sum(M)-sum(M[Ichange])+sum(M2)
#                    if abs(HD-HD2)>1e-14:
#                        print(HD,HD2)

                if HD<HD0:
                    print('Changing I')
                    E[ik]=E[ik][II2[III[Iold]]]
                    I[ik]=I[ik][II2[III[Iold]]]
                    #x11=x11[II2[III[Iold]]]
                    #x11=x110[I[ik]]
                    HD0=HD
                    #M[Ichange]=M2
                III=array(range(nband))
                III[i2:i2+2]=III[i2:i2+2][::-1]
            II2=argsort(I[ik])
            Iold=I[ik]
        I=I[:,argsort(amin(E,0))]
        for (ik, II) in enumerate(I):
            E[ik]=E[ik][II]
        save('WAVECAR_order.npy',I)
    return I

def sort_2D(folder,E,k):
    if os.path.isfile('WAVECAR_order.npy') and os.path.getmtime('WAVECAR_order.npy') > os.path.getmtime('WAVECAR'):
        Isort=load('WAVECAR_order.npy')
        for (i, II) in enumerate(Isort):
            E[i]=E[i][II]
    else:
        n=get_ngx(folder)
        nband=rW.get_nband(folder)
        nk=size(E,0)

        nangle= 1
        angles=arccos(k[:,0]/sqrt(sum(k**2,1)))
        angles[k[:,1]<0]=2*pi-angles[k[:,1]<0]
        Ik=[]
        Ikk=[]
        Ik_old=[]
        j2=[]
        j2s=[]
        Ikk.append(0)
    
        #HD0=zeros(size(qpoints,0))
        for iangle in range(1*nangle):
            angle1=2*pi*(mod(iangle,nangle))/nangle
            angle2=2*pi*(mod(iangle,nangle)+1)/nangle
            Ik_old=array(r_[Ik_old,Ik])
            Ik=array(where((angles>=angle1)*(angles<angle2)*(sum(k**2,1)!=0)))
            Ik.shape=-1
            I1=argsort(sum(k[Ik]**2,1))
            Ik=Ik[I1]
            Ik=range(nk)
            Ik=array(r_[Ik,Ik],dtype=int)
            I2=array(r_[Ik_old,Ik],dtype=int)
            for j in array(r_[range(size(Ik_old)+1,size(Ik_old)+size(k[Ik],0))
                ],dtype=int):
                j2.append(I2[argsort(sum((k[I2[:j+1]]-k[I2[j]])**2,1))[1+iangle/nangle]])
                d0=sum((k[0]-k[1])**2)
                j2s.append(I2[where(sum((k[I2[:j]]-k[I2[j]])**2,1)
                        <1.1*sum((k[j2[-1]]-k[I2[j]])**2))[0]])
                Ikk.append(I2[j])
        Ik=array(r_[Ik_old,Ik],dtype=int)
        Ik=array(Ikk)
#        Ik=range(nk)
#        j2=range(nk)
#        j2=[]
#        for j in range(1,nk):
#            j2.append(argsort(sum((k[Ik[:j+1]]-k[Ik[j]])**2,1))[1])
        j2=array(j2,dtype=int)
        #print(shape(Ik),shape(j2),shape(Ikk))

        Isort=outer(ones(nk,dtype=int),range(nband))

        Iold=array(range(nband))
        II2=argsort(Iold)
        for (iik, ik) in enumerate(Ik[1:]):
            print('from '+str(ik)+' to '+str(j2[iik]))
            III=array(range(nband))
            Iold=Isort[j2[iik]]
            II2=argsort(Iold)
            Isort[ik]=Iold.copy()
            E[ik]=E[ik][Iold]

            x11=rW.wave_real(n,ik,-1)
            x11.shape=(size(x11,0),-1)
            x11=x11/reshape(sqrt(sum(abs(x11)**2,1)),(-1,1))
            D1=E[ik]

            x2=[]
            print(j2s[iik])
            for (ij2, jj2) in enumerate(j2s[iik]):
            #for (ij2, jj2) in enumerate([j2[iik]]):#j2s[iik]:
                x2.append(rW.wave_real(n,jj2,-1))
                x2[ij2].shape=(size(x2,1),-1)
                x2[ij2]=x2[ij2]/reshape(sqrt(sum(abs(x2[ij2])**2,1)),(-1,1))
#                x3=x2[ij2]
#                D2=E[jj2]
#                h1=dot(x11.conj(),x3.T)
#                H1=dot(h1.conj().T,dot(diag(D1),h1))
#                h2=dot(x2[ij2].conj(),x3.T)
#                H2=dot(h2.conj().T,dot(diag(D2),h2))
#                x4=(H1-H2)
#                DD=reshape(D2,(-1,1))-D2
#                DD[DD!=0.]=1/DD[DD!=0.]
#                x5=x4*DD
#                x3=x3+dot(x5.T,x3)
#                x3=x3/reshape(sqrt(sum(abs(x3)**2,1)),(-1,1))
#                x2[ij2]=x3

            #x3=x2

            #M=-abs(sum(x11.conj()*x3,1))**2
            for i2 in range(nband):
                HD=0.
                x1=x11[Isort[ik][II2[III]]].conj()
                for (ij2,xj) in enumerate(x2):
                    HD+=-sum(abs(sum(x1*xj,1))**2)
                if i2==0:
                    #HD=sum(M)
                    HD0=HD
#                else:
#                    #M3=-abs(sum(x1.conj()*x2,1))**2
#                    Ichange=Isort[ik][II2[i2-1:i2+1]]
#                    M2=-abs(sum(x11[Ichange[::-1]].conj()*x3[Ichange],1))**2
                    #HD=sum(M)-sum(M[Ichange])+sum(M2)

                if HD<HD0:
                    print('Changing I')
                    E[ik]=E[ik][II2[III[Iold]]]
                    Isort[ik]=Isort[ik][II2[III[Iold]]]
                    #x11=x11[II2[III[Iold]]]
                    #x11=x110[I[ik]]
                    HD0=HD
                    #M[Ichange]=M2
                III=array(range(nband))
                III[i2:i2+2]=III[i2:i2+2][::-1]
        Isort=Isort[:,argsort(amin(E,0))]
        for (ik, II) in enumerate(Isort):
            E[ik]=E[ik][II]
        save('WAVECAR_order.npy',Isort)
    return Isort

def get_nelect(folder='.',fix_spin=False):
    if not folder[-4:]=='.xml':
        folder=folder+'/vasprun.xml'
    tree=ET.parse(folder)
    root = tree.getroot()
    nelec=int(float(root.find('parameters').find("separator[@name='electronic']").find("i[@name='NELECT']").text))
    if fix_spin:
       if 'F' in root.find('parameters').find("separator[@name='electronic']").find("separator[@name='electronic spin']").find("i[@name='LNONCOLLINEAR']").text:
           nelec=nelec/2
    return nelec
    

def get_E(folder='.',sort=False,return_nelec=False):
    if not folder[-4:]=='.xml':
        folder=folder+'/vasprun.xml'
    tree=ET.parse(folder)
    root = tree.getroot()
    kpts=root.find('calculation').find('eigenvalues').find('array').find('set').find('set').findall('set')
    
    EF=float(root.find('calculation').find('dos').find('i').text)
    ks=[]
    E=[]
    for kpt in kpts:
        E.append([float(Ei.text.split()[0]) for Ei in kpt.findall('r')])
    for kpoints in root.find('kpoints').find('varray').findall('v'):
        ks.append([float(s) for s in kpoints.text.split()])
    ks=array(ks)
    b=[]
    for bi in root.find('structure').find('crystal').find("varray[@name='rec_basis']").findall('v'):
        b.append([float(s) for s in bi.text.split()])
    #b=2*pi*array(b)
    b=2*pi*array(b)
    ks=dot(ks,b)

    E=array(E)

    if sort:
        E0=E.copy()
        if sort=='2D':
            I=sort_2D(folder,E,ks)
        else:
            I=sort_1D(folder,E)
        el=(E,EF,ks)
#        E2=array([Ei[I[iE]] for (iE, Ei) in enumerate(E0)])
#        return (E2,EF,ks)
    #ks=array(ks)*2*pi/a
    else:
        el=(E,EF,ks)

    if return_nelec:
        return el+(get_nelect(folder),)
    else:
        return el

def get_dos(folder='.'):
    folder=folder+['/vasprun.xml',''][folder[-4:]=='.xml']
    tree=ET.parse(folder)
    root = tree.getroot()
    dosi=root.find('calculation').find('dos').find('total').find('array').find('set').findall('set')
    EF=float(root.find('calculation').find('dos').find('i').text)
    dos=[[],[]]
    for ii, dosii in enumerate(dosi):
        dos[ii]=[[float(xii) for xii in xi.text.split()] for xi in dosii.findall('r')]
        dos[ii]=array(dos[ii])
        dos[ii][:,0]=dos[ii][:,0]-EF

    return dos

def get_pdos(folder='.'):
    folder=folder+['/vasprun.xml',''][folder[-4:]=='.xml']
    tree=ET.parse(folder)
    root = tree.getroot()
    dosi=root.find('calculation').find('dos').find('partial').find('array').find('set').findall('set')
    dos=[]
    for ii, dosii in enumerate(dosi):
        dos.append([])
        for iii, dosiii in enumerate(dosii.findall('set')):
            dos[-1].append(array([[float(xii) for xii in xi.text.split()] for xi in dosiii.findall('r')]))
    return dos

    

def get_pos(folder):
    tree=ET.parse(folder+'/data-file.xml')
    root = tree.getroot()
    a=float(root.find('CELL').find('LATTICE_PARAMETER').text)
    nions=int(root.find('IONS').find('NUMBER_OF_ATOMS').text)

    tau=[]
    for i in range(nions):
        tau.append([float(s) for s in root.find('IONS').find('ATOM.%d' % (i+1)).get('tau').split()])
    return array(tau)/a

def get_phon_disp(filename='disp.yaml',return_pos=False):
    data=try_yaml_load(filename)
    natom=data['natom']
    ncell=data['displacements'][6]['atom']-data['displacements'][0]['atom']
    disp=zeros((size(data['displacements']),3*natom/ncell))
    for j, v in enumerate(data['displacements']):
        disp[j,(v['atom']-1)/ncell*3:(v['atom']-1)/ncell*3+3]=array(v['displacement'])
    disp=array(disp)

    if not return_pos:
        return disp
    else:
        ats=[]
        for v in data['displacements']:
            ats.append(int(v['atom'])-1)

        pos=[]
        for at in data['atoms']:
            pos.append(at['position'])
        lat=data['lattice']
        return disp, pos, lat, ats

def fold_brillouin(qpoints,b):
    GG=arange(-2,3)
    G=array([kron(kron(GG,GG**0),GG**0),
        kron(kron(GG**0,GG),GG**0),
        kron(kron(GG**0,GG**0),GG)]).T
    qpoints3=qpoints.copy()
    for xi in G:
        qpoints2=qpoints-dot(xi,b)
        I=sum((qpoints2)**2,1)<sum(qpoints3**2,1)
        qpoints3[I]=qpoints2[I]
    return qpoints3

def get_phon_mass(filename='qpoints.yaml',return_pos=False):
    data=try_yaml_load(filename)
    mass=[]
    for v in data['atom-info']:
        mass.append(float(v['mass']))
    if return_pos:
        pos=[]
        for p in data['position']:
            pos.append(p)
        lat=[]
        for ai in data['real-basis']:
            lat.append(ai)
        pos=dot(pos,lat)
        return array(mass), array(pos), array(lat)
    else:
        return array(mass)

def sort_phon_1D(qpoints,frequencies,eigvecs):
    if os.path.isfile('qpoints_order.npy') and os.path.getmtime('qpoints_order.npy') > os.path.getmtime('qpoints.yaml'):
        I=load('qpoints_order.npy')
        for (i, II) in enumerate(I):
            frequencies[i]=frequencies[i][II]
            eigvecs[i]=eigvecs[i][II]
    else:
        print('Sorting '+str(size(qpoints,0))+' eigenvectors')
        I=outer(ones(size(qpoints,0),dtype=int),range(size(eigvecs,2)))
        for j in range(1,size(qpoints,0)):
            II1=argsort(frequencies[j])
            II2=argsort(frequencies[j-1])
            III=range(size(eigvecs,2))
            I[j][II2]=I[j][II1]
            frequencies[j][II2]=frequencies[j][II1]
            eigvecs[j][II2]=eigvecs[j][II1]
            for i2 in range(size(eigvecs,2)):
                HD=0.
                x1=eigvecs[j][II2[III]]
                x2=eigvecs[j-1][II2]
                x3=x2.T
#                D1=frequencies[j][II2[III]]**2
#                D2=frequencies[j-1][II2]**2
#                H1=dot(D1*x1.T,x1.conj())
#                H2=dot(D2*x2.T,x2.conj())
#                x4=dot(x2.conj(),dot((H1-H2),x2.T))
#                DD=reshape(D2,(-1,1))-D2
#                DD[DD!=0.]=1/DD[DD!=0.]
#                x4*=DD
#                x3-=dot(x2.T,x4)
#                x3=x3/sqrt(sum(abs(x3)**2,0))
                HD= -sum(abs(diag(dot(x1.conj(),x3)))**2)
                #print(i2,HD)
                if i2==0:
                    HD0=HD
                if HD<HD0:
                    print('Changing order '+str(i2))
                    frequencies[j][II2]=frequencies[j][II2[III]]
                    eigvecs[j][II2]=eigvecs[j][II2[III]]
                    I[j][II2]=I[j][II2[III]]
                    HD0=HD
                III=range(size(eigvecs,2))
                III[i2:i2+2]=III[i2:i2+2][::-1]
        save('qpoints_order.npy',I)
    return frequencies, eigvecs



def sort_phon_2D(qpoints,frequencies,eigvecs):
    if os.path.isfile('qpoints_order.npy') and os.path.getmtime('qpoints_order.npy') > os.path.getmtime('qpoints.yaml'):
        I=load('qpoints_order.npy')
        for (i, II) in enumerate(I):
            frequencies[i]=frequencies[i][II]
            eigvecs[i]=eigvecs[i][II]
    else:
        Isort=outer(ones(size(qpoints,0),dtype=int),range(size(eigvecs,2)))
        nangle= 4
        angles=arccos(qpoints[:,0]/sqrt(sum(qpoints**2,1)))
        angles[qpoints[:,1]<0]=2*pi-angles[qpoints[:,1]<0]
        I=[]
        I_old=[]
    
        #HD0=zeros(size(qpoints,0))
        for iangle in range(1*nangle):
            angle1=2*pi*(mod(iangle,nangle))/nangle
            angle2=2*pi*(mod(iangle,nangle)+1)/nangle
            I_old=array(r_[I_old,I])
            I=array(where((angles>=angle1)*(angles<angle2)*(sum(qpoints**2,1)!=0)))
            I.shape=-1
            I1=argsort(sum(qpoints[I]**2,1))
            I=I[I1]
            I2=array(r_[I_old,I],dtype=int)
            for j in array(r_[range(size(I_old)+1,size(I_old)+size(qpoints[I],0))
                ],dtype=int):
                j2=argsort(sum((qpoints[I2[:j+1]]-qpoints[I2[j]])**2,1))[1+iangle/nangle]
                d0=sum((qpoints[0]-qpoints[1])**2)
                j2s=where(sum((qpoints[I2[:j]]-qpoints[I2[j]])**2,1)
                        <1.1*sum((qpoints[I2[j2]]-qpoints[I2[j]])**2))[0]
                II1=argsort(frequencies[I2[j]])
                III=range(size(eigvecs,2))
                II2=argsort(frequencies[I2[j2s[0]]])
                Isort[I2[j]][II2]=Isort[I2[j]][II1]
                frequencies[I2[j]][II2]=frequencies[I2[j]][II1]
                eigvecs[I2[j]][II2]=eigvecs[I2[j]][II1]
                for i2 in range(size(eigvecs,2)):
                    HD=0.
                    for j2 in j2s:
                        D1=frequencies[I2[j]][II2[III]]**2
                        D2=frequencies[I2[j2]][II2]**2
                        x1=eigvecs[I2[j]][II2[III]]
                        x2=eigvecs[I2[j2]][II2]
                        H1=dot(D1*x1.T,x1.conj())
                        H2=dot(D2*x2.T,x2.conj())
                        x3=x2.T
                        x4=dot(x2.conj(),dot((H1-H2),x2.T))
                        DD=reshape(D2,(-1,1))-D2
                        DD[DD!=0.]=1/DD[DD!=0.]
                        x4*=DD
                        x3-=dot(x2.T,x4)
                        x3=x3/sqrt(sum(abs(x3)**2,0))
                        HD+=-sum(abs(diag(dot(x1.conj(),x3)))**2)
                    if i2==0:
                        HD0=HD
                    if HD<HD0:
                        frequencies[I2[j]][II2]=frequencies[I2[j]][II2[III]]
                        eigvecs[I2[j]][II2]=eigvecs[I2[j]][II2[III]]
                        Isort[I2[j]][II2]=Isort[I2[j]][II2[III]]
                        HD0=HD
                    III=range(size(eigvecs,2))
                    III[i2:i2+2]=III[i2:i2+2][::-1]
        save('qpoints_order.npy',Isort)
        print('qpoints order saved')


def get_phon_data(filename='band.yaml',return_eigvecs=False,sort=False,fold=False):
    data=try_yaml_load(filename)
    segment_positions = []
    frequencies = []
#    distances = []
    qpoints = []
    #npoints = data['nqpoint'] / data['npath']
    eigvecs=[]

    for j, v in enumerate(data['phonon']):
        frequencies.append([f['frequency'] for f in v['band']])
        if return_eigvecs or sort:
            #eigvecs.append([f['eigenvector_time_aligned'] for f in v['band']])
            eigvecs.append([f['eigenvector'] for f in v['band']])
#        distances.append(v['distance'])
        qpoints.append(v['q-position'])

#        if j % npoints == 0:
#            segment_positions.append(v['distance'])

    frequencies=array(frequencies)
    qpoints=array(qpoints)
    try:
        b=[]
        for bi in data['reciprocal-basis']:
            b.append(array(bi))
        b=2*pi*array(b)
        qpoints=dot(qpoints,b)
    except:
        pass

    if return_eigvecs or sort:
        eigvecs=array(eigvecs)
        eigvecs=eigvecs[:,:,:,:,0]+1j*eigvecs[:,:,:,:,1]
        eigvecs.shape=(size(eigvecs,0),size(eigvecs,1),size(eigvecs,2)*size(eigvecs,3))


        if sort=='1D':
            frequencies, eigvecs= sort_phon_1D(qpoints,frequencies,eigvecs)
        elif sort:
            sort_phon_2D(qpoints,frequencies,eigvecs)

    if fold:
        qpoints=fold_brillouin(qpoints,b)
                
    if not return_eigvecs:
        return qpoints, frequencies#, segment_positions
    else:
        return qpoints, frequencies, eigvecs#, segment_positions

if __name__=="__main__":
    import argparse
    import pickle
    
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('folder',nargs='?',default='./out/mat.save')
    args=parser.parse_args()
    get_phon_data('qpoints.yaml')
    get_phon_disp()
    #pickle.dump(get_E(args.folder),open(args.folder+'/E.pickle','wb'))
